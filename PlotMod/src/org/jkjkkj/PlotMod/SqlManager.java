package org.jkjkkj.PlotMod;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import org.jkjkkj.PlotMod.utils.UUIDFetcher;

public class SqlManager {

    private static Connection conn = null;

    public final static String sqlitedb = "/plots.db";


    private final static String PLOT_TABLE = "CREATE TABLE `plotmodPlots` (" + 
            "`idX` INTEGER," + 
            "`idZ` INTEGER," + 
            "`owner` varchar(100) NOT NULL," + 
            "`world` varchar(32) NOT NULL DEFAULT '0'," + 
            "`topX` INTEGER NOT NULL DEFAULT '0'," + 
            "`bottomX` INTEGER NOT NULL DEFAULT '0'," + 
            "`topZ` INTEGER NOT NULL DEFAULT '0'," + 
            "`bottomZ` INTEGER NOT NULL DEFAULT '0'," + 
            "`biome` varchar(32) NOT NULL DEFAULT '0'," + 
            "`expireddate` DATETIME NULL," + 
            "`finished` boolean NOT NULL DEFAULT '0'," + 
            "`finisheddate` varchar(16) NULL," + 
            "`protected` boolean NOT NULL DEFAULT '0'," + 
            "`ownerId` blob(16)," + 
            "PRIMARY KEY (idX, idZ, world));";

    private final static String COMMENT_TABLE = "CREATE TABLE `plotmodComments` (" + 
            "`idX` INTEGER," + 
            "`idZ` INTEGER," + 
            "`world` varchar(32) NOT NULL," + 
            "`commentid` INTEGER," + 
            "`player` varchar(32) NOT NULL," +
            "`comment` text," + 
            "`playerid` blob(16)," + 
            "PRIMARY KEY (idX, idZ, world, commentid));";

    private final static String ALLOWED_TABLE = "CREATE TABLE `plotmodAllowed` (" + 
            "`idX` INTEGER," + 
            "`idZ` INTEGER," + 
            "`world` varchar(32) NOT NULL," + 
            "`player` varchar(32) NOT NULL," +
            "`playerid` blob(16)," +
            "PRIMARY KEY (idX, idZ, world, player));";

    private final static String DENIED_TABLE = "CREATE TABLE `plotmodDenied` (" + 
            "`idX` INTEGER," + 
            "`idZ` INTEGER," + 
            "`world` varchar(32) NOT NULL," + 
            "`player` varchar(32) NOT NULL," +
            "`playerid` blob(16)," +
            "PRIMARY KEY (idX, idZ, world, player));";

    public static Connection initialize() {
        try {
            if (PlotMod.usemySQL) {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(PlotMod.mySQLconn, PlotMod.mySQLuname, PlotMod.mySQLpass);
                conn.setAutoCommit(false);
            } else {
                Class.forName("org.sqlite.JDBC");
                conn = DriverManager.getConnection("jdbc:sqlite:" + PlotMod.configpath + "/plots.db");
                conn.setAutoCommit(false);
            }
        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + "SQL exception on initialize :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            PlotMod.logger.severe(PlotMod.NAME + "You need the SQLite/MySQL library. :");
            PlotMod.logger.severe("  " + ex.getMessage());
        }

        createTable();

        return conn;
    }

    public static String getSchema() {
        String conn = PlotMod.mySQLconn;

        if (conn.lastIndexOf("/") > 0)
            return conn.substring(conn.lastIndexOf("/") + 1);
        else
            return "";
    }

    public static void UpdateTables() {
        Statement statement = null;
        ResultSet set = null;

        try {
            Connection conn = getConnection();

            statement = conn.createStatement();

            String schema = getSchema();

            if (PlotMod.usemySQL) {

                set = statement.executeQuery("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" + schema + "' AND " + "TABLE_NAME='plotmodPlots' AND column_name='finisheddate'");
                if (!set.next()) {
                    statement.execute("ALTER TABLE plotmodPlots ADD finisheddate varchar(16) NULL;");
                    conn.commit();
                }
                set.close();

                set = statement.executeQuery("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" + schema + "' AND " + "TABLE_NAME='plotmodPlots' AND column_name='protected'");
                if (!set.next()) {
                    statement.execute("ALTER TABLE plotmodPlots ADD protected boolean NOT NULL DEFAULT '0';");
                    conn.commit();
                }
                set.close();
                
                set = statement.executeQuery("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" + schema + "' AND " + "TABLE_NAME='plotmodPlots' AND column_name='ownerid'");
                if (!set.next()) {
                    statement.execute("ALTER TABLE plotmodPlots ADD ownerid blob(16) NULL;");
                    conn.commit();
                }
                set.close();

                set = statement.executeQuery("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" + schema + "' AND " + "TABLE_NAME='plotmodAllowed' AND column_name='playerid'");
                if (!set.next()) {
                    statement.execute("ALTER TABLE plotmodAllowed ADD playerid blob(16) NULL;");
                    conn.commit();
                }
                set.close();

                set = statement.executeQuery("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" + schema + "' AND " + "TABLE_NAME='plotmodDenied' AND column_name='playerid'");
                if (!set.next()) {
                    statement.execute("ALTER TABLE plotmodDenied ADD playerid blob(16) NULL;");
                    conn.commit();
                }
                set.close();

                set = statement.executeQuery("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" + schema + "' AND " + "TABLE_NAME='plotmodComments' AND column_name='playerid'");
                if (!set.next()) {
                    statement.execute("ALTER TABLE plotmodComments ADD playerid blob(16) NULL;");
                    conn.commit();
                }
                set.close();


            } else {
                String column;
                boolean found = false;

                // FinishedDate
                set = statement.executeQuery("PRAGMA table_info(`plotmodPlots`)");

                while (set.next() && !found) {
                    column = set.getString(2);
                    if (column.equalsIgnoreCase("finisheddate"))
                        found = true;
                }

                if (!found) {
                    statement.execute("ALTER TABLE plotmodPlots ADD finisheddate varchar(16) NULL;");
                    conn.commit();
                }
                set.close();
                found = false;

                // Protected
                set = statement.executeQuery("PRAGMA table_info(`plotmodPlots`)");

                while (set.next() && !found) {
                    column = set.getString(2);
                    if (column.equalsIgnoreCase("protected"))
                        found = true;
                }

                if (!found) {
                    statement.execute("ALTER TABLE plotmodPlots ADD protected boolean NOT NULL DEFAULT '0';");
                    conn.commit();
                }
                set.close();
                found = false;

                // OwnerId
                set = statement.executeQuery("PRAGMA table_info(`plotmodPlots`)");

                while (set.next() && !found) {
                    column = set.getString(2);
                    if (column.equalsIgnoreCase("ownerid"))
                        found = true;
                }

                if (!found) {
                    statement.execute("ALTER TABLE plotmodPlots ADD ownerid blob(16) NULL;");
                    conn.commit();
                }
                set.close();
                found = false;

                // Allowed id
                set = statement.executeQuery("PRAGMA table_info(`plotmodAllowed`)");

                while (set.next() && !found) {
                    column = set.getString(2);
                    if (column.equalsIgnoreCase("playerid"))
                        found = true;
                }

                if (!found) {
                    statement.execute("ALTER TABLE plotmodAllowed ADD playerid blob(16) NULL;");
                    conn.commit();
                }
                set.close();
                found = false;

                // Denied id
                set = statement.executeQuery("PRAGMA table_info(`plotmodDenied`)");

                while (set.next() && !found) {
                    column = set.getString(2);
                    if (column.equalsIgnoreCase("playerid"))
                        found = true;
                }

                if (!found) {
                    statement.execute("ALTER TABLE plotmodDenied ADD playerid blob(16) NULL;");
                    conn.commit();
                }
                set.close();
                found = false;

                // Commenter id
                set = statement.executeQuery("PRAGMA table_info(`plotmodComments`)");

                while (set.next() && !found) {
                    column = set.getString(2);
                    if (column.equalsIgnoreCase("playerid"))
                        found = true;
                }

                if (!found) {
                    statement.execute("ALTER TABLE plotmodComments ADD playerid blob(16) NULL;");
                    conn.commit();
                }
                set.close();
                found = false;

            }
        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Update table exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (set != null)
                    set.close();
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Update table exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    public static Connection getConnection() {
        if (conn == null)
            conn = initialize();
        if (PlotMod.usemySQL) {
            try {
                if (!conn.isValid(10))
                    conn = initialize();
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + "Failed to check SQL status :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
        return conn;
    }

    public static void closeConnection() {
        if (conn != null) {
            try {
                if (PlotMod.usemySQL) {
                    if (conn.isValid(10)) {
                        conn.close();
                    }
                    conn = null;
                } else {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + "Error on Connection close :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    private static boolean tableExists(String name) {
        ResultSet rs = null;
        try {
            Connection conn = getConnection();

            DatabaseMetaData dbm = conn.getMetaData();
            rs = dbm.getTables(null, null, name, null);
            if (!rs.next())
                return false;
            return true;
        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Table Check Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
            return false;
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Table Check SQL Exception (on closing) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    private static void createTable() {
        Statement st = null;
        try {
            Connection conn = getConnection();
            st = conn.createStatement();

            if (!tableExists("plotmodPlots")) {
                st.executeUpdate(PLOT_TABLE);
                conn.commit();
            }

            if (!tableExists("plotmodComments")) {
                st.executeUpdate(COMMENT_TABLE);
                conn.commit();
            }

            if (!tableExists("plotmodAllowed")) {
                st.executeUpdate(ALLOWED_TABLE);
                conn.commit();
            }

            if (!tableExists("plotmodDenied")) {
                st.executeUpdate(DENIED_TABLE);
                conn.commit();
            }

            UpdateTables();

            if (PlotMod.usemySQL) {
                File sqlitefile = new File(PlotMod.configpath + sqlitedb);
                if (!sqlitefile.exists()) {
                    return;
                } else {
                    PlotMod.logger.info(PlotMod.NAME + " Modifying database for MySQL support");
                    PlotMod.logger.info(PlotMod.NAME + " Trying to import plots from plots.db");
                    Class.forName("org.sqlite.JDBC");
                    Connection sqliteconn = DriverManager.getConnection("jdbc:sqlite:" + PlotMod.configpath + sqlitedb);
                    sqliteconn.setAutoCommit(false);
                    Statement slstatement = sqliteconn.createStatement();
                    ResultSet setPlots = slstatement.executeQuery("SELECT * FROM plotmodPlots");
                    Statement slAllowed = sqliteconn.createStatement();
                    ResultSet setAllowed = null;
                    Statement slDenied = sqliteconn.createStatement();
                    ResultSet setDenied = null;
                    Statement slComments = sqliteconn.createStatement();
                    ResultSet setComments = null;

                    int size = 0;
                    while (setPlots.next()) {
                        int idX = setPlots.getInt("idX");
                        int idZ = setPlots.getInt("idZ");
                        String owner = setPlots.getString("owner");
                        String world = setPlots.getString("world").toLowerCase();
                        int topX = setPlots.getInt("topX");
                        int bottomX = setPlots.getInt("bottomX");
                        int topZ = setPlots.getInt("topZ");
                        int bottomZ = setPlots.getInt("bottomZ");
                        String biome = setPlots.getString("biome");
                        java.sql.Date expireddate = setPlots.getDate("expireddate");
                        boolean finished = setPlots.getBoolean("finished");
                        PlayerList allowed = new PlayerList();
                        PlayerList denied = new PlayerList();
                        List<String[]> comments = new ArrayList<String[]>();
                        String finisheddate = setPlots.getString("finisheddate");
                        boolean protect = setPlots.getBoolean("protected");

                        byte[] byOwner = setPlots.getBytes("ownerId");

                        UUID ownerId = null;

                        if (byOwner != null) {
                            ownerId = UUIDFetcher.fromBytes(byOwner);
                        }

                        setAllowed = slAllowed.executeQuery("SELECT * FROM plotmodAllowed WHERE idX = '" + idX + "' AND idZ = '" + idZ + "' AND world = '" + world + "'");

                        while (setAllowed.next()) {
                            byte[] byPlayerId = setAllowed.getBytes("playerid");
                            if (byPlayerId == null) {
                                allowed.put(setAllowed.getString("player"));
                            } else {
                                allowed.put(setAllowed.getString("player"), UUIDFetcher.fromBytes(byPlayerId));
                            }
                        }

                        if (setAllowed != null) {
                            setAllowed.close();
                        }

                        setDenied = slDenied.executeQuery("SELECT * FROM plotmodDenied WHERE idX = '" + idX + "' AND idZ = '" + idZ + "' AND world = '" + world + "'");

                        while (setDenied.next()) {
                            byte[] byPlayerId = setDenied.getBytes("playerid");
                            if (byPlayerId == null) {
                                denied.put(setDenied.getString("player"));
                            } else {
                                denied.put(setDenied.getString("player"), UUIDFetcher.fromBytes(byPlayerId));
                            }
                        }

                        if (setDenied != null) {
                            setDenied.close();
                        }

                        setComments = slComments.executeQuery("SELECT * FROM plotmodComments WHERE idX = '" + idX + "' AND idZ = '" + idZ + "' AND world = '" + world + "'");

                        while (setComments.next()) {
                            String[] comment = new String[3];

                            byte[] byPlayerId = setComments.getBytes("playerid");
                            if (byPlayerId != null) {
                                comment[2] = UUIDFetcher.fromBytes(byPlayerId).toString();
                            } else {
                                comment[2] = null;
                            }

                            comment[0] = setComments.getString("player");
                            comment[1] = setComments.getString("comment");
                            comments.add(comment);
                        }

                        Plot plot = new Plot(owner, ownerId, world, topX, bottomX, topZ, bottomZ, biome, expireddate, finished, allowed, comments, "" + idX + ";" + idZ, finisheddate, protect, denied);
                        addPlot(plot, idX, idZ, topX, bottomX, topZ, bottomZ);

                        size++;
                    }
                    PlotMod.logger.info(PlotMod.NAME + " Imported " + size + " plots from " + sqlitedb);
                    if (slstatement != null)
                        slstatement.close();
                    if (slAllowed != null)
                        slAllowed.close();
                    if (slComments != null)
                        slComments.close();
                    if (slDenied != null)
                        slDenied.close();
                    if (setPlots != null)
                        setPlots.close();
                    if (setComments != null)
                        setComments.close();
                    if (setAllowed != null)
                        setAllowed.close();
                    if (sqliteconn != null)
                        sqliteconn.close();

                    PlotMod.logger.info(PlotMod.NAME + " Renaming " + sqlitedb + " to " + sqlitedb + ".old");
                    if (!sqlitefile.renameTo(new File(PlotMod.configpath, sqlitedb + ".old"))) {
                        PlotMod.logger.warning(PlotMod.NAME + " Failed to rename " + sqlitedb + "! Please rename this manually!");
                    }
                }
            }
        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Create Table Exception :");
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " You need the SQLite library :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Could not create the table (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    public static void addPlot(Plot plot, int idX, int idZ, World w) {
        addPlot(plot, idX, idZ, PlotManager.topX(plot.id, w), PlotManager.bottomX(plot.id, w), PlotManager.topZ(plot.id, w), PlotManager.bottomZ(plot.id, w));
    }

    public static void addPlot(Plot plot, int idX, int idZ, int topX, int bottomX, int topZ, int bottomZ) {
        PreparedStatement ps = null;
        Connection conn;
        StringBuilder strSql = new StringBuilder();

        // Plots
        try {
            conn = getConnection();

            strSql.append("INSERT INTO plotmodPlots (idX, idZ, owner, world, topX, bottomX, topZ, bottomZ, ");
            strSql.append("biome, expireddate, finished, finisheddate, protected, ownerId) ");
            strSql.append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            ps = conn.prepareStatement(strSql.toString());
            ps.setInt(1, idX);
            ps.setInt(2, idZ);
            ps.setString(3, plot.owner);
            ps.setString(4, plot.world.toLowerCase());
            ps.setInt(5, topX);
            ps.setInt(6, bottomX);
            ps.setInt(7, topZ);
            ps.setInt(8, bottomZ);
            ps.setString(9, plot.biome.name());
            ps.setDate(10, plot.expireddate);
            ps.setBoolean(11, plot.finished);
            ps.setString(12, null);
            ps.setBoolean(13, plot.protect);
            if (plot.ownerId != null) {
                ps.setBytes(14, UUIDFetcher.toBytes(plot.ownerId));
            } else {
                ps.setBytes(14, null);
            }

            ps.executeUpdate();
            conn.commit();

            if (plot.allowed != null && plot.allowed.getAllPlayers() != null) {
                HashMap<String, UUID> allowed = plot.allowed.getAllPlayers();
                for (String key : allowed.keySet()) {
                    addPlotAllowed(key, allowed.get(key), idX, idZ, plot.world);
                }
            }

            if (plot.denied != null && plot.denied.getAllPlayers() != null) {
                HashMap<String, UUID> denied = plot.denied.getAllPlayers();
                for (String key : denied.keySet()) {
                    addPlotDenied(key, denied.get(key), idX, idZ, plot.world);
                }
            }

            if (plot.comments != null && plot.comments.size() > 0) {
                int commentid = 1;
                for (String[] comments : plot.comments) {
                    String strUUID = "";
                    UUID uuid = null;

                    if (comments.length >= 3) {
                        strUUID = comments[2];
                        try {
                            uuid = UUID.fromString(strUUID);
                        } catch (Exception e) {
                        }
                    }

                    addPlotComment(comments, commentid, idX, idZ, plot.world, uuid);
                    commentid++;
                }
            }

            if (plot.owner != null && !plot.owner.equals("") && plot.ownerId == null) {
                fetchOwnerUUIDAsync(idX, idZ, plot.world.toLowerCase(), plot.owner);
            }


        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Insert Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Insert Exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    public static void updatePlot(int idX, int idZ, String world, String field, Object value) {
        PreparedStatement ps = null;
        Connection conn;

        // Plots
        try {
            conn = getConnection();

            ps = conn.prepareStatement("UPDATE plotmodPlots SET " + field + " = ? " + "WHERE idX = ? AND idZ = ? AND world = ?");

            if (value instanceof UUID) {
                ps.setBytes(1, UUIDFetcher.toBytes((UUID) value));
            } else {
                ps.setObject(1, value);
            }
            ps.setInt(2, idX);
            ps.setInt(3, idZ);
            ps.setString(4, world.toLowerCase());

            ps.executeUpdate();
            conn.commit();

            if (field.equalsIgnoreCase("owner")) {
                fetchOwnerUUIDAsync(idX, idZ, world, value.toString());
            }

        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Insert Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Insert Exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    public static void updateTable(String tablename, int idX, int idZ, String world, String field, Object value) {
        PreparedStatement ps = null;
        Connection conn;

        // Plots
        try {
            conn = getConnection();

            ps = conn.prepareStatement("UPDATE " + tablename + " SET " + field + " = ? " + "WHERE idX = ? AND idZ = ? AND world = ?");

            if (value instanceof UUID) {
                ps.setBytes(1, UUIDFetcher.toBytes((UUID) value));
            } else {
                ps.setObject(1, value);
            }
            ps.setInt(2, idX);
            ps.setInt(3, idZ);
            ps.setString(4, world.toLowerCase());

            ps.executeUpdate();
            conn.commit();

            if (field.equalsIgnoreCase("owner")) {
                fetchOwnerUUIDAsync(idX, idZ, world, value.toString());
            } else if (field.equalsIgnoreCase("player")) {
                if (tablename.equalsIgnoreCase("plotmodallowed")) {

                }
            }

        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Insert Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Insert Exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    public static void addPlotAllowed(String player, int idX, int idZ, String world) {
        addPlotAllowed(player, null, idX, idZ, world);
    }

    public static void addPlotAllowed(String player, UUID playerid, int idX, int idZ, String world) {
        PreparedStatement ps = null;
        Connection conn;

        // Allowed
        try {
            conn = getConnection();

            ps = conn.prepareStatement("INSERT INTO plotmodAllowed (idX, idZ, player, world, playerid) " + "VALUES (?,?,?,?,?)");

            ps.setInt(1, idX);
            ps.setInt(2, idZ);
            ps.setString(3, player);
            ps.setString(4, world.toLowerCase());
            if (playerid != null) {
                ps.setBytes(5, UUIDFetcher.toBytes(playerid));
            } else {
                ps.setBytes(5, null);
            }

            ps.executeUpdate();
            conn.commit();

            if (playerid == null) {
                fetchAllowedUUIDAsync(idX, idZ, world, player);
            }

        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Insert Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Insert Exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    public static void addPlotDenied(String player, UUID playerid, int idX, int idZ, String world) {
        PreparedStatement ps = null;
        Connection conn;

        // Denied
        try {
            conn = getConnection();

            ps = conn.prepareStatement("INSERT INTO plotmodDenied (idX, idZ, player, world, playerid) " + "VALUES (?,?,?,?,?)");

            ps.setInt(1, idX);
            ps.setInt(2, idZ);
            ps.setString(3, player);
            ps.setString(4, world.toLowerCase());
            if (playerid != null) {
                ps.setBytes(5, UUIDFetcher.toBytes(playerid));
            } else {
                ps.setBytes(5, null);
            }

            ps.executeUpdate();
            conn.commit();

            if (playerid == null) {
                fetchDeniedUUIDAsync(idX, idZ, world, player);
            }

        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Insert Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Insert Exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }
    
    public static void addPlotComment(String[] comment, int commentid, int idX, int idZ, String world) {
        UUID uuid = null;
        if(comment.length > 2) {
            try{
                uuid = UUID.fromString(comment[2]);
            }catch(IllegalArgumentException e){}
        }
        addPlotComment(comment, commentid, idX, idZ, world, uuid);
    }

    public static void addPlotComment(String[] comment, int commentid, int idX, int idZ, String world, UUID uuid) {
        PreparedStatement ps = null;
        Connection conn;

        // Comments
        try {
            conn = getConnection();

            ps = conn.prepareStatement("INSERT INTO plotmodComments (idX, idZ, commentid, player, comment, world, playerid) " + "VALUES (?,?,?,?,?,?,?)");
            
            ps.setInt(1, idX);
            ps.setInt(2, idZ);
            ps.setInt(3, commentid);
            ps.setString(4, comment[0]);
            ps.setString(5, comment[1]);
            ps.setString(6, world.toLowerCase());
            if (uuid != null) {
                ps.setBytes(7, UUIDFetcher.toBytes(uuid));
            } else {
                ps.setBytes(7, null);
            }

            ps.executeUpdate();
            conn.commit();

        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Insert Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Insert Exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    public static void deletePlot(int idX, int idZ, String world) {
        PreparedStatement ps = null;
        ResultSet set = null;
        try {
            Connection conn = getConnection();

            ps = conn.prepareStatement("DELETE FROM plotmodComments WHERE idX = ? and idZ = ? and LOWER(world) = ?");
            ps.setInt(1, idX);
            ps.setInt(2, idZ);
            ps.setString(3, world);
            ps.executeUpdate();
            conn.commit();
            ps.close();

            ps = conn.prepareStatement("DELETE FROM plotmodAllowed WHERE idX = ? and idZ = ? and LOWER(world) = ?");
            ps.setInt(1, idX);
            ps.setInt(2, idZ);
            ps.setString(3, world);
            ps.executeUpdate();
            conn.commit();
            ps.close();

            ps = conn.prepareStatement("DELETE FROM plotmodDenied WHERE idX = ? and idZ = ? and LOWER(world) = ?");
            ps.setInt(1, idX);
            ps.setInt(2, idZ);
            ps.setString(3, world);
            ps.executeUpdate();
            conn.commit();
            ps.close();

            ps = conn.prepareStatement("DELETE FROM plotmodPlots WHERE idX = ? and idZ = ? and LOWER(world) = ?");
            ps.setInt(1, idX);
            ps.setInt(2, idZ);
            ps.setString(3, world);
            ps.executeUpdate();
            conn.commit();
            ps.close();

        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Delete Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (set != null) {
                    set.close();
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Delete Exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    public static void deletePlotComment(int idX, int idZ, int commentid, String world) {
        PreparedStatement ps = null;
        ResultSet set = null;
        try {
            Connection conn = getConnection();

            ps = conn.prepareStatement("DELETE FROM plotmodComments WHERE idX = ? and idZ = ? and commentid = ? and LOWER(world) = ?");
            ps.setInt(1, idX);
            ps.setInt(2, idZ);
            ps.setInt(3, commentid);
            ps.setString(4, world);
            ps.executeUpdate();
            conn.commit();

        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Delete Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (set != null) {
                    set.close();
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Delete Exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    public static void deletePlotAllowed(int idX, int idZ, String player, UUID playerid, String world) {
        PreparedStatement ps = null;
        ResultSet set = null;

        try {
            Connection conn = getConnection();

            if (playerid == null) {
                ps = conn.prepareStatement("DELETE FROM plotmodAllowed WHERE idX = ? and idZ = ? and player = ? and LOWER(world) = ?");
                ps.setString(3, player);
            } else {
                ps = conn.prepareStatement("DELETE FROM plotmodAllowed WHERE idX = ? and idZ = ? and playerid = ? and LOWER(world) = ?");
                ps.setBytes(3, UUIDFetcher.toBytes(playerid));
            }
            ps.setInt(1, idX);
            ps.setInt(2, idZ);
            ps.setString(4, world);
            ps.executeUpdate();
            conn.commit();

        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Delete Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (set != null) {
                    set.close();
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Delete Exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    public static void deletePlotDenied(int idX, int idZ, String player, UUID playerid, String world) {
        PreparedStatement ps = null;
        ResultSet set = null;

        try {
            Connection conn = getConnection();

            if (playerid == null) {
                ps = conn.prepareStatement("DELETE FROM plotmodDenied WHERE idX = ? and idZ = ? and player = ? and LOWER(world) = ?");
                ps.setString(3, player);
            } else {
                ps = conn.prepareStatement("DELETE FROM plotmodDenied WHERE idX = ? and idZ = ? and playerid = ? and LOWER(world) = ?");
                ps.setBytes(3, UUIDFetcher.toBytes(playerid));
            }
            ps.setInt(1, idX);
            ps.setInt(2, idZ);
            ps.setString(4, world);
            ps.executeUpdate();
            conn.commit();

        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Delete Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (set != null) {
                    set.close();
                }
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Delete Exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
    }

    public static HashMap<String, Plot> getPlots(String world) {
        HashMap<String, Plot> ret = new HashMap<String, Plot>();
        Statement statementPlot = null;
        Statement statementAllowed = null;
        Statement statementDenied = null;
        Statement statementComment = null;
        ResultSet setPlots = null;
        ResultSet setAllowed = null;
        ResultSet setDenied = null;
        ResultSet setComments = null;

        try {
            Connection conn = getConnection();

            statementPlot = conn.createStatement();
            setPlots = statementPlot.executeQuery("SELECT * FROM plotmodPlots WHERE LOWER(world) = '" + world + "'");
            int size = 0;
            while (setPlots.next()) {
                size++;
                int idX = setPlots.getInt("idX");
                int idZ = setPlots.getInt("idZ");
                String owner = setPlots.getString("owner");
                int topX = setPlots.getInt("topX");
                int bottomX = setPlots.getInt("bottomX");
                int topZ = setPlots.getInt("topZ");
                int bottomZ = setPlots.getInt("bottomZ");
                String biome = setPlots.getString("biome");
                java.sql.Date expireddate = setPlots.getDate("expireddate");
                boolean finished = setPlots.getBoolean("finished");
                PlayerList allowed = new PlayerList();
                PlayerList denied = new PlayerList();
                List<String[]> comments = new ArrayList<String[]>();
                String finisheddate = setPlots.getString("finisheddate");
                boolean protect = setPlots.getBoolean("protected");

                byte[] byOwner = setPlots.getBytes("ownerId");

                UUID ownerId = null;

                if (byOwner != null) {
                    ownerId = UUIDFetcher.fromBytes(byOwner);
                }
                

                statementAllowed = conn.createStatement();
                setAllowed = statementAllowed.executeQuery("SELECT * FROM plotmodAllowed WHERE idX = '" + idX + "' AND idZ = '" + idZ + "' AND LOWER(world) = '" + world + "'");

                while (setAllowed.next()) {
                    byte[] byPlayerId = setAllowed.getBytes("playerid");
                    if (byPlayerId == null) {
                        allowed.put(setAllowed.getString("player"));
                    } else {
                        allowed.put(setAllowed.getString("player"), UUIDFetcher.fromBytes(byPlayerId));
                    }
                }

                if (setAllowed != null)
                    setAllowed.close();

                statementDenied = conn.createStatement();
                setDenied = statementDenied.executeQuery("SELECT * FROM plotmodDenied WHERE idX = '" + idX + "' AND idZ = '" + idZ + "' AND LOWER(world) = '" + world + "'");

                while (setDenied.next()) {
                    byte[] byPlayerId = setDenied.getBytes("playerid");
                    if (byPlayerId == null) {
                        denied.put(setDenied.getString("player"));
                    } else {
                        denied.put(setDenied.getString("player"), UUIDFetcher.fromBytes(byPlayerId));
                    }
                }

                if (setDenied != null)
                    setDenied.close();

                statementComment = conn.createStatement();
                setComments = statementComment.executeQuery("SELECT * FROM plotmodComments WHERE idX = '" + idX + "' AND idZ = '" + idZ + "' AND LOWER(world) = '" + world + "'");

                while (setComments.next()) {
                    String[] comment = new String[3];
                    comment[0] = setComments.getString("player");
                    comment[1] = setComments.getString("comment");
                    
                    byte[] byPlayerId = setComments.getBytes("playerid");
                    if (byPlayerId != null) {
                        comment[2] = UUIDFetcher.fromBytes(byPlayerId).toString();
                    }
                    
                    comments.add(comment);
                }

                Plot plot = new Plot(owner, ownerId, world, topX, bottomX, topZ, bottomZ, biome, expireddate, finished, allowed, comments, "" + idX + ";" + idZ, finisheddate, protect, denied);
                ret.put("" + idX + ";" + idZ, plot);
            }
            PlotMod.logger.info(PlotMod.NAME + " " + size + " plots loaded");
        } catch (SQLException ex) {
            PlotMod.logger.severe(PlotMod.NAME + " Load Exception :");
            PlotMod.logger.severe("  " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (statementPlot != null)
                    statementPlot.close();
                if (statementAllowed != null)
                    statementAllowed.close();
                if (statementComment != null)
                    statementComment.close();
                if (setPlots != null)
                    setPlots.close();
                if (setComments != null)
                    setComments.close();
                if (setAllowed != null)
                    setAllowed.close();
            } catch (SQLException ex) {
                PlotMod.logger.severe(PlotMod.NAME + " Load Exception (on close) :");
                PlotMod.logger.severe("  " + ex.getMessage());
            }
        }
        return ret;
    }

    public static void plotConvertToUUIDAsynchronously() {
        Bukkit.getServer().getScheduler().runTaskAsynchronously(PlotMod.self, new Runnable() {
            @Override
            public void run() {
                PlotMod.logger.info("Checking if conversion to UUID needed...");

                boolean boConversion = false;
                Statement statementPlayers = null;
                PreparedStatement psOwnerId = null;
                PreparedStatement psAllowedPlayerId = null;
                PreparedStatement psDeniedPlayerId = null;
                PreparedStatement psCommentsPlayerId = null;
                
                PreparedStatement psDeleteOwner = null;
                PreparedStatement psDeleteAllowed = null;
                PreparedStatement psDeleteDenied = null;
                PreparedStatement psDeleteComments = null;
                
                ResultSet setPlayers = null;
                int nbConverted = 0;
                String sql = "";
                int count = 0;

                try {
                    Connection conn = getConnection();

                    // Get all the players
                    statementPlayers = conn.createStatement();
                    // Exclude groups and names with * or missing
                    sql = "SELECT LOWER(owner) as Name FROM plotmodPlots WHERE NOT owner IS NULL AND Not owner LIKE 'group:%' AND Not owner LIKE '%*%' AND ownerid IS NULL GROUP BY LOWER(owner) ";
                    sql = sql + "UNION SELECT LOWER(player) as Name FROM plotmodAllowed WHERE NOT player IS NULL AND Not player LIKE 'group:%' AND Not player LIKE '%*%' AND playerid IS NULL GROUP BY LOWER(player) ";
                    sql = sql + "UNION SELECT LOWER(player) as Name FROM plotmodDenied WHERE NOT player IS NULL AND Not player LIKE 'group:%' AND Not player LIKE '%*%' AND playerid IS NULL GROUP BY LOWER(player) ";
                    sql = sql + "UNION SELECT LOWER(player) as Name FROM plotmodComments WHERE NOT player IS NULL AND Not player LIKE 'group:%' AND Not player LIKE '%*%' AND playerid IS NULL GROUP BY LOWER(player)";

                    PlotMod.logger.info("Verifying if database needs conversion");

                    setPlayers = statementPlayers.executeQuery(sql);

                    if (setPlayers.next()) {

                        List<String> names = new ArrayList<String>();
                        
                        //Prepare delete statements
                        psDeleteOwner = conn.prepareStatement("UPDATE plotmodPlots SET owner = '' WHERE owner = ? ");
                        psDeleteAllowed = conn.prepareStatement("DELETE FROM plotmodAllowed WHERE player = ? ");
                        psDeleteDenied = conn.prepareStatement("DELETE FROM plotmodDenied WHERE player = ? ");
                        psDeleteComments = conn.prepareStatement("DELETE FROM plotmodComments WHERE player = ? ");

                        PlotMod.logger.info("Starting to convert plots to UUID");
                        do {
                            String name = setPlayers.getString("Name");
                            if (!name.equals("")) {     
                                if(name.matches("^[a-zA-Z0-9_]{1,16}$")) {
                                    names.add(name);
                                } else {
                                    PlotMod.logger.warning("Invalid name found : " + name + ". Removing from database.");
                                    psDeleteOwner.setString(1, name);
                                    psDeleteOwner.executeUpdate();
                                    psDeleteAllowed.setString(1, name);
                                    psDeleteAllowed.executeUpdate();
                                    psDeleteDenied.setString(1, name);
                                    psDeleteDenied.executeUpdate();
                                    psDeleteComments.setString(1, name);
                                    psDeleteComments.executeUpdate();
                                    conn.commit();
                                }
                            }
                        } while (setPlayers.next());

                        psDeleteOwner.close();
                        psDeleteAllowed.close();
                        psDeleteDenied.close();
                        psDeleteComments.close();
                        
                        UUIDFetcher fetcher = new UUIDFetcher(names);

                        Map<String, UUID> response = null;

                        try {
                            PlotMod.logger.info("Fetching " + names.size() + " UUIDs from Mojang servers...");
                            response = fetcher.call();
                            PlotMod.logger.info("Finished fetching " + response.size() + " UUIDs. Starting database update.");
                        } catch (Exception e) {
                            PlotMod.logger.warning("Exception while running UUIDFetcher");
                            e.printStackTrace();
                        }

                        if (response.size() > 0) {
                            psOwnerId = conn.prepareStatement("UPDATE plotmodPlots SET ownerid = ? WHERE LOWER(owner) = ? AND ownerid IS NULL");
                            psAllowedPlayerId = conn.prepareStatement("UPDATE plotmodAllowed SET playerid = ? WHERE LOWER(player) = ? AND playerid IS NULL");
                            psDeniedPlayerId = conn.prepareStatement("UPDATE plotmodDenied SET playerid = ? WHERE LOWER(player) = ? AND playerid IS NULL");
                            psCommentsPlayerId = conn.prepareStatement("UPDATE plotmodComments SET playerid = ? WHERE LOWER(player) = ? AND playerid IS NULL");

                            for (String key : response.keySet()) {
                                count = 0;
                                // Owner
                                psOwnerId.setBytes(1, UUIDFetcher.toBytes(response.get(key)));
                                psOwnerId.setString(2, key.toLowerCase());
                                count += psOwnerId.executeUpdate();
                                // Allowed
                                psAllowedPlayerId.setBytes(1, UUIDFetcher.toBytes(response.get(key)));
                                psAllowedPlayerId.setString(2, key.toLowerCase());
                                count += psAllowedPlayerId.executeUpdate();
                                // Denied
                                psDeniedPlayerId.setBytes(1, UUIDFetcher.toBytes(response.get(key)));
                                psDeniedPlayerId.setString(2, key.toLowerCase());
                                count += psDeniedPlayerId.executeUpdate();
                                // Commenter
                                psCommentsPlayerId.setBytes(1, UUIDFetcher.toBytes(response.get(key)));
                                psCommentsPlayerId.setString(2, key.toLowerCase());
                                psCommentsPlayerId.executeUpdate();
                                conn.commit();
                                if (count > 0) {
                                    nbConverted++;
                                } else {
                                    PlotMod.logger.warning("Unable to update player '" + key + "'");
                                }
                            }

                            psOwnerId.close();
                            psAllowedPlayerId.close();
                            psDeniedPlayerId.close();
                            psCommentsPlayerId.close();
                            
                            
                            //Update plot information
                            for (PlotMapInfo pmi : PlotMod.plotmaps.values()) {
                                for(Plot plot : pmi.plots.values()) {
                                    for(Entry<String, UUID> player : response.entrySet()) {
                                        
                                    	//Owner
                                        if(plot.ownerId == null && plot.owner != null && plot.owner.equalsIgnoreCase(player.getKey())) {
                                            plot.owner = player.getKey();
                                            plot.ownerId = player.getValue();
                                        }
                                        
                                        //Allowed
                                        plot.allowed.replace(player.getKey(), player.getValue());
                                        
                                        //Denied
                                        plot.denied.replace(player.getKey(), player.getValue());
                                        
                                        //Comments
                                        for(String[] comment : plot.comments) {
                                            if(comment.length > 2 && comment[2] == null && comment[0] != null && comment[0].equalsIgnoreCase(player.getKey())) {
                                                comment[0] = player.getKey();
                                                comment[2] = player.getValue().toString();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        boConversion = true;
                        PlotMod.logger.info(nbConverted + " players converted");
                    }
                    setPlayers.close();
                    statementPlayers.close();

                    if (boConversion) {
                        PlotMod.logger.info("Plot conversion finished");
                    } else {
                        PlotMod.logger.info("No plot conversion needed");
                    }
                } catch (SQLException ex) {
                    PlotMod.logger.severe("Conversion to UUID failed :");
                    PlotMod.logger.severe("  " + ex.getMessage());
                    for (StackTraceElement e : ex.getStackTrace()) {
                        PlotMod.logger.severe("  " + e.toString());
                    }
                } finally {
                    try {
                        if (statementPlayers != null) {
                            statementPlayers.close();
                        }
                        if (psOwnerId != null) {
                            psOwnerId.close();
                        }
                        if (psAllowedPlayerId != null) {
                            psAllowedPlayerId.close();
                        }
                        if (psDeniedPlayerId != null) {
                            psDeniedPlayerId.close();
                        }
                        if (psCommentsPlayerId != null) {
                            psCommentsPlayerId.close();
                        }
                        if (setPlayers != null) {
                            setPlayers.close();
                        }
                        if (psDeleteOwner != null) {
                            psDeleteOwner.close();
                        }
                        if (psDeleteAllowed != null) {
                            psDeleteAllowed.close();
                        }
                        if (psDeleteDenied != null) {
                            psDeleteDenied.close();
                        }
                        if (psDeleteComments != null) {
                            psDeleteComments.close();
                        }
                    } catch (SQLException ex) {
                        PlotMod.logger.severe("Conversion to UUID failed (on close) :");
                        PlotMod.logger.severe("  " + ex.getMessage());
                        for (StackTraceElement e : ex.getStackTrace()) {
                            PlotMod.logger.severe("  " + e.toString());
                        }
                    }
                }
            }
        });
    }

    public static void fetchOwnerUUIDAsync(int idX, int idZ, String world, String owner) {
        _fetchUUIDAsync(idX, idZ, world, "owner", owner);
    }

    public static void fetchAllowedUUIDAsync(int idX, int idZ, String world, String allowed) {
        _fetchUUIDAsync(idX, idZ, world, "allowed", allowed);
    }

    public static void fetchDeniedUUIDAsync(int idX, int idZ, String world, String denied) {
        _fetchUUIDAsync(idX, idZ, world, "denied", denied);
    }

    private static void _fetchUUIDAsync(final int idX, final int idZ, final String world, final String Property, final String name) {
        if (PlotMod.self.initialized) {
            Bukkit.getServer().getScheduler().runTaskAsynchronously(PlotMod.self, new Runnable() {
                @Override
                public void run() {
    
                    PreparedStatement ps = null;
    
                    try {
                        Connection conn = getConnection();
    
                        @SuppressWarnings("deprecation")
                        Player p = Bukkit.getPlayerExact(name);
                        UUID uuid = null;
                        String newname = name;
    
                        if (p != null) {
                            uuid = p.getUniqueId();
                            newname = p.getName();
                        } else {
                            List<String> names = new ArrayList<String>();
    
                            names.add(name);
    
                            UUIDFetcher fetcher = new UUIDFetcher(names);
    
                            Map<String, UUID> response = null;
    
                            try {
                                PlotMod.logger.info("Fetching " + names.size() + " UUIDs from Mojang servers...");
                                response = fetcher.call();
                                PlotMod.logger.info("Received " + response.size() + " UUIDs. Starting database update...");
                                
                                if (response.size() > 0) {
                                    uuid = response.values().toArray(new UUID[0])[0];
                                    newname = response.keySet().toArray(new String[0])[0];
                                }
                            } catch (IOException e) {
                                PlotMod.logger.warning("Unable to connect to Mojang server!");
                            } catch (Exception e) {
                                PlotMod.logger.warning("Exception while running UUIDFetcher");
                                e.printStackTrace();
                            }
                        }
    
                        
                        if (Property.equals("owner")) {
                            ps = conn.prepareStatement("UPDATE plotmodPlots SET ownerid = ?, owner = ? WHERE LOWER(owner) = ? AND idX = '" + idX + "' AND idZ = '" + idZ + "' AND LOWER(world) = '" + world + "'");
                        } else if (Property.equals("allowed")){
                            ps = conn.prepareStatement("UPDATE plotmodAllowed SET playerid = ?, player = ? WHERE LOWER(player) = ? AND idX = '" + idX + "' AND idZ = '" + idZ + "' AND LOWER(world) = '" + world + "'");
                        } else if (Property.equals("denied")){
                            ps = conn.prepareStatement("UPDATE plotmodDenied SET playerid = ?, player = ? WHERE LOWER(player) = ? AND idX = '" + idX + "' AND idZ = '" + idZ + "' AND LOWER(world) = '" + world + "'");
                        } else {
                        	return;
                        }
                        
    
                        if (uuid != null) {
                            ps.setBytes(1, UUIDFetcher.toBytes(uuid));
                        } else {
                            ps.setBytes(1, null);
                        }
                        ps.setString(2, newname);
                        ps.setString(3, name.toLowerCase());
                        ps.executeUpdate();
                        conn.commit();
    
                        ps.close();
                        
                        if (uuid != null) {
                            Plot plot = PlotManager.getPlotById(world, "" + idX + ";" + idZ);
                            
                            if(plot != null) {
                            	
                            	
                            	if (Property.equals("owner")){
                            		plot.owner = newname;
                                    plot.ownerId = uuid;
                            	} else if (Property.equals("allowed")){
                            		plot.allowed.remove(name);
                                    plot.allowed.put(newname, uuid);
                            	} else if (Property.equals("denied")){
                            		plot.denied.remove(name);
                                    plot.denied.put(newname, uuid);
                            	} else {
                            		return;
                            	}
                            	
                                
                            }
                            
                            if(p == null) {
                                PlotMod.logger.info("UUID updated to Database!");
                            }
                        }
                    } catch (SQLException ex) {
                        PlotMod.logger.severe("Conversion to UUID failed :");
                        PlotMod.logger.severe("  " + ex.getMessage());
                        for (StackTraceElement e : ex.getStackTrace()) {
                            PlotMod.logger.severe("  " + e.toString());
                        }
                    } finally {
                        try {
                            if (ps != null) {
                                ps.close();
                            }
                        } catch (SQLException ex) {
                            PlotMod.logger.severe("Conversion to UUID failed (on close) :");
                            PlotMod.logger.severe("  " + ex.getMessage());
                            for (StackTraceElement e : ex.getStackTrace()) {
                                PlotMod.logger.severe("  " + e.toString());
                            }
                        }
                    }
                }
            });
        }
    }
    
    public static void updatePlotsNewUUID(final UUID uuid, final String newname) {
        Bukkit.getServer().getScheduler().runTaskAsynchronously(PlotMod.self, new Runnable() {
            @Override
            public void run() {
                PreparedStatement[] pss = new PreparedStatement[4];

                try {
                    Connection conn = getConnection();

                    pss[0] = conn.prepareStatement("UPDATE plotmodPlots SET owner = ? WHERE ownerid = ?");
                    pss[1] = conn.prepareStatement("UPDATE plotmodAllowed SET player = ? WHERE playerid = ?");
                    pss[2] = conn.prepareStatement("UPDATE plotmodDenied SET player = ? WHERE playerid = ?");
                    pss[3] = conn.prepareStatement("UPDATE plotmodComments SET player = ? WHERE playerid = ?");

                    for (PreparedStatement ps : pss) {
                        ps.setString(1, newname);
                        ps.setBytes(2, UUIDFetcher.toBytes(uuid));
                        ps.executeUpdate();
                    }

                    conn.commit();

                    for (PreparedStatement ps : pss) {
                        ps.close();
                    }

                } catch (SQLException ex) {
                    PlotMod.logger.severe("Update player in database from uuid failed :");
                    PlotMod.logger.severe("  " + ex.getMessage());
                    for (StackTraceElement e : ex.getStackTrace()) {
                        PlotMod.logger.severe("  " + e.toString());
                    }
                } finally {
                    try {
                        for (PreparedStatement ps : pss) {
                            if (ps != null) {
                                ps.close();
                            }
                        }
                    } catch (SQLException ex) {
                        PlotMod.logger.severe("Update player in database from uuid failed (on close) :");
                        PlotMod.logger.severe("  " + ex.getMessage());
                        for (StackTraceElement e : ex.getStackTrace()) {
                            PlotMod.logger.severe("  " + e.toString());
                        }
                    }
                }
            }
        });
    }
}
