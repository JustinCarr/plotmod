package org.jkjkkj.PlotMod;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.PlotMod.listener.PlotDenyListener;
import org.jkjkkj.PlotMod.listener.PlotListener;
import org.jkjkkj.PlotMod.listener.PlotWorldEditListener;
import org.yaml.snakeyaml.Yaml;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.universicraft_mp.TypicalChat.Vars;

public class PlotMod extends JavaPlugin
{
	//get rid of world teleporting for good
	
	//Add a way to remove comments, and to stop people from infinitely commenting
	
	//Add an inventory for the /plot tpworld command
	// make limits dynamic to world
	
	public static String NAME;
	public static String PLUGIN_PREFIX;
	public static String NOTIFY_PREFIX;
	public static String VERSION;
	
	public static Logger logger = Logger.getLogger("Minecraft");
		
	public static Boolean usemySQL;
    public static String mySQLuname;
    public static String mySQLpass;
    public static String mySQLconn;
    public static String configpath;
    public static Boolean advancedlogging;
    public static String language;
    public static Boolean autoUpdate;
    public static Boolean allowToDeny;
    
    public static ConcurrentHashMap<String, PlotMapInfo> plotmaps = null;
    
    public static WorldEditPlugin we = null;
    
    private static HashSet<String> playersignoringwelimit = null;
    private static HashMap<String, String> captions;
    public static ArrayList<Material> preventeditems = null;
    public static ArrayList<Material> protectedblocks = null;
    
    public static World worldcurrentlyprocessingexpired;
    public static CommandSender cscurrentlyprocessingexpired;
    public static Integer counterexpired;
    public static Integer nbperdeletionprocessingexpired;
    
    protected static PlotMod self = null;
    
    Boolean initialized = false;
	
	public void onDisable()
	{	
		SqlManager.closeConnection();
		NAME = null;
		PLUGIN_PREFIX = null;
		PLUGIN_PREFIX = null;
		VERSION = null;
		
		logger = null;
		
		usemySQL = null;
		mySQLuname = null;
		mySQLpass = null;
		mySQLconn = null;
		advancedlogging = null;
		language = null;
		autoUpdate = null;
		plotmaps = null;
		configpath = null;
		we = null;
		playersignoringwelimit = null;
		captions = null;
		preventeditems = null;
		protectedblocks = null;
		worldcurrentlyprocessingexpired = null;
		cscurrentlyprocessingexpired = null;
		counterexpired = null;
		nbperdeletionprocessingexpired = null;
		self = null;
		allowToDeny = null;
		initialized = null;
	}
	
	public void onEnable()
	{
	    self = this;
	    
	    if (!isChatThere()){
	    	shutDownPlugin("TypicalChat wasn't found... shutting down plugin.");
	    }
	    
	    initialize();
				
		PluginManager pm = getServer().getPluginManager();
				
		pm.registerEvents(new PlotListener(), this);
		
		
		
		if(pm.getPlugin("WorldEdit") != null)
		{
			we = (WorldEditPlugin) pm.getPlugin("WorldEdit");
			pm.registerEvents(new PlotWorldEditListener(), this);			
		}
		
		if(allowToDeny)
		{
			pm.registerEvents(new PlotDenyListener(), this);
		}
				
		getCommand("plots").setExecutor(new PMCommand(this));
		
		initialized = true;
		
		
		
		SqlManager.plotConvertToUUIDAsynchronously();
	}

	
	
	public ChunkGenerator getDefaultWorldGenerator(String worldname, String id)
	{		
		if(PlotManager.isPlotWorld(worldname))
		{
			return new PlotGen(PlotManager.getMap(worldname));
		}
		else
		{
			logger.warning(PLUGIN_PREFIX + "Configuration not found for Plots world '" + worldname + "' Using defaults");
			return new PlotGen();
		}
	}
	
	public static boolean hasPerms(CommandSender sender, String node)
	{
		return sender.hasPermission(node);
	}
	
	public static void shutDownPlugin(String reason){
		
		logger.severe(PLUGIN_PREFIX + reason);
		Bukkit.getPluginManager().disablePlugin(self);
		
	}
	
	public void initialize()
	{
		PluginDescriptionFile pdfFile = this.getDescription();
		NAME = pdfFile.getName();
		PLUGIN_PREFIX = Vars.Neg + "[" + Vars.Pos + "Plots" + Vars.Neg + "] " + Vars.Ntrl;
		NOTIFY_PREFIX = Vars.NOTICE_PREFIX + Vars.Ntrl;
		VERSION = pdfFile.getVersion();
		configpath = getDataFolder().getAbsolutePath();
		playersignoringwelimit = new HashSet<String>();

		if(!this.getDataFolder().exists()) 
		{
        	this.getDataFolder().mkdirs();
        }
				
		File configfile = new File(configpath, "config.yml");
		FileConfiguration config = new YamlConfiguration();
		
		try 
		{
			config.load(configfile);
		} 
		catch (FileNotFoundException e) {} 
		catch (IOException e) 
		{
			logger.severe(PLUGIN_PREFIX + " Configuration file could not be read!");
			e.printStackTrace();
		} 
		catch (InvalidConfigurationException e) 
		{
			logger.severe(PLUGIN_PREFIX + " The configuration format is invalid!");
			e.printStackTrace();
		}
        
        usemySQL = config.getBoolean("usemySQL", false);
		mySQLconn = config.getString("mySQLconn", "jdbc:mysql://localhost:3306/minecraft");
		mySQLuname = config.getString("mySQLuname", "root");
		mySQLpass = config.getString("mySQLpass", "password");
		advancedlogging = config.getBoolean("AdvancedLogging", false);
		language = config.getString("Language", "english");
		autoUpdate = config.getBoolean("auto-update", false);
		allowToDeny = config.getBoolean("allowToDeny", true);
		
		setPreventedItems();
		setProtectedBlocks();

		ConfigurationSection worlds;
		
		if(!config.contains("worlds"))
		{
			worlds = config.createSection("worlds");
			
			ConfigurationSection plotworld = worlds.createSection("plotworld");
			
			plotworld.set("PlotAutoLimit", 1000);
			plotworld.set("PathWidth", 7);
			plotworld.set("PlotSize", 32);
			
			plotworld.set("BottomBlockId", "7");
			plotworld.set("WallBlockId", "44");
			plotworld.set("PlotFloorBlockId", "2");
			plotworld.set("PlotFillingBlockId", "3");
			plotworld.set("RoadMainBlockId", "5");
			plotworld.set("RoadStripeBlockId", "5:2");
			
			plotworld.set("RoadHeight", 64);
			plotworld.set("DaysToExpiration", 7);
			plotworld.set("ProtectedWallBlockId", "44:4");
			plotworld.set("AutoLinkPlots", true);
			plotworld.set("DisableExplosion", true);
			plotworld.set("DisableIgnition", true);
						
			
			worlds.set("plotworld", plotworld);
			config.set("worlds", worlds);
		}
		else
		{
			worlds = config.getConfigurationSection("worlds");
		}
		
		plotmaps = new ConcurrentHashMap<String, PlotMapInfo>();
		
		for(String worldname : worlds.getKeys(false))
		{
			PlotMapInfo tempPlotInfo = new PlotMapInfo();
			ConfigurationSection currworld = worlds.getConfigurationSection(worldname);
			
			tempPlotInfo.PlotAutoLimit = currworld.getInt("PlotAutoLimit", 100);
			tempPlotInfo.PathWidth = currworld.getInt("PathWidth", 7);
			tempPlotInfo.PlotSize = currworld.getInt("PlotSize", 32);
			
			tempPlotInfo.BottomBlockId = getBlockId(currworld, "BottomBlockId", "7:0");
			tempPlotInfo.BottomBlockValue = getBlockValue(currworld, "BottomBlockId", "7:0");
			tempPlotInfo.WallBlockId = getBlockId(currworld, "WallBlockId", "44:0");
			tempPlotInfo.WallBlockValue = getBlockValue(currworld, "WallBlockId", "44:0");
			tempPlotInfo.PlotFloorBlockId = getBlockId(currworld, "PlotFloorBlockId", "2:0");
			tempPlotInfo.PlotFloorBlockValue = getBlockValue(currworld, "PlotFloorBlockId", "2:0");
			tempPlotInfo.PlotFillingBlockId = getBlockId(currworld, "PlotFillingBlockId", "3:0");
			tempPlotInfo.PlotFillingBlockValue = getBlockValue(currworld, "PlotFillingBlockId", "3:0");
			tempPlotInfo.RoadMainBlockId = getBlockId(currworld, "RoadMainBlockId", "5:0");
			tempPlotInfo.RoadMainBlockValue = getBlockValue(currworld, "RoadMainBlockId", "5:0");
			tempPlotInfo.RoadStripeBlockId = getBlockId(currworld, "RoadStripeBlockId", "5:2");
			tempPlotInfo.RoadStripeBlockValue = getBlockValue(currworld, "RoadStripeBlockId", "5:2");
			
			tempPlotInfo.RoadHeight = currworld.getInt("RoadHeight", currworld.getInt("WorldHeight", 64));
			if(tempPlotInfo.RoadHeight > 250)
			{
				logger.severe(PLUGIN_PREFIX + "RoadHeight above 250 is unsafe. This is the height at which your road is located. Setting it to 64.");
				tempPlotInfo.RoadHeight = 64;
			}
			tempPlotInfo.DaysToExpiration = currworld.getInt("DaysToExpiration", 7);
						
			tempPlotInfo.ProtectedWallBlockId = currworld.getString("ProtectedWallBlockId", "44:4");
			tempPlotInfo.AutoLinkPlots = currworld.getBoolean("AutoLinkPlots", true);
			tempPlotInfo.DisableExplosion = currworld.getBoolean("DisableExplosion", true);
			tempPlotInfo.DisableIgnition = currworld.getBoolean("DisableIgnition", true);
			
			currworld.set("PlotAutoLimit", tempPlotInfo.PlotAutoLimit);
			currworld.set("PathWidth", tempPlotInfo.PathWidth);
			currworld.set("PlotSize", tempPlotInfo.PlotSize);
			
			currworld.set("BottomBlockId", getBlockIdValue(tempPlotInfo.BottomBlockId, tempPlotInfo.BottomBlockValue));
			currworld.set("WallBlockId", getBlockIdValue(tempPlotInfo.WallBlockId, tempPlotInfo.WallBlockValue));
			currworld.set("PlotFloorBlockId", getBlockIdValue(tempPlotInfo.PlotFloorBlockId, tempPlotInfo.PlotFloorBlockValue));
			currworld.set("PlotFillingBlockId", getBlockIdValue(tempPlotInfo.PlotFillingBlockId, tempPlotInfo.PlotFillingBlockValue));
			currworld.set("RoadMainBlockId", getBlockIdValue(tempPlotInfo.RoadMainBlockId, tempPlotInfo.RoadMainBlockValue));
			currworld.set("RoadStripeBlockId", getBlockIdValue(tempPlotInfo.RoadStripeBlockId, tempPlotInfo.RoadStripeBlockValue));
			
			currworld.set("RoadHeight", tempPlotInfo.RoadHeight);
			currworld.set("WorldHeight", null);
			currworld.set("DaysToExpiration", tempPlotInfo.DaysToExpiration);
			currworld.set("ProtectedWallBlockId", tempPlotInfo.ProtectedWallBlockId);
			currworld.set("AutoLinkPlots", tempPlotInfo.AutoLinkPlots);
			currworld.set("DisableExplosion", tempPlotInfo.DisableExplosion);
			currworld.set("DisableIgnition", tempPlotInfo.DisableIgnition);
			
			worlds.set(worldname, currworld);
			
			tempPlotInfo.plots = SqlManager.getPlots(worldname.toLowerCase());
			
			plotmaps.put(worldname.toLowerCase(), tempPlotInfo);
		}
		
		config.set("usemySQL", usemySQL);
		config.set("mySQLconn", mySQLconn);
		config.set("mySQLuname", mySQLuname);
		config.set("mySQLpass", mySQLpass);
		config.set("AdvancedLogging", advancedlogging);
		config.set("Language", language);
		config.set("auto-update", autoUpdate);
		config.set("allowToDeny", allowToDeny);
		
		try 
		{
			config.save(configfile);
		} 
		catch (IOException e) 
		{
			logger.severe(PLUGIN_PREFIX + " Error writting configurations");
			e.printStackTrace();
		}
		
		if (Vars.donor_plotworld != null){
			World w = Bukkit.getWorld(Vars.donor_plotworld);
			if (w != null){
				for (Plot plot : PlotManager.getPlots(w).values()){
					if (plot.allowedcount() != 0){
						plot.removeAllAllowed();
					}
				}
			}
		}
		
		
		for (Player p : getServer().getOnlinePlayers()){
			if (p.hasPermission("plots.admin.weanywhere")){
				addIgnoreWELimit(p);
			}
		}
		createIgnoreWELimitCheck();
		
		loadCaptions();
    }
	
	public void createIgnoreWELimitCheck(){
		
		getServer().getScheduler().scheduleSyncRepeatingTask(this, new BukkitRunnable(){
			
			public void run(){
				
				for (Player p : Bukkit.getOnlinePlayers()){
					if (p.hasPermission("plots.admin.weanywhere")){
						if (!PlotMod.playersignoringwelimit.contains(p.getName())){
							PlotMod.addIgnoreWELimit(p);
						}
					} else {
						PlotMod.removeIgnoreWELimit(p);
					}
				}
				
			}
		}, 1200L, 1200L);
		
	}
	
	public static void addIgnoreWELimit(Player p)
	{
		if(!playersignoringwelimit.contains(p.getName()))
		{
			playersignoringwelimit.add(p.getName());
			if(we != null)
				PlotWorldEdit.removeMask(p);
		}
	}
	
	public static void removeIgnoreWELimit(Player p)
	{
		if(playersignoringwelimit.contains(p.getName()))
		{
			playersignoringwelimit.remove(p.getName());
			if(we != null)
				PlotWorldEdit.setMask(p);
		}
	}
	
	public static boolean isIgnoringWELimit(Player p)
	{
		if(hasPerms(p, "plots.admin.weanywhere"))
		{
			if (!playersignoringwelimit.contains(p.getName()))
			{
				playersignoringwelimit.add(p.getName());
			}
			return true;
		}
		else
		{
			if (playersignoringwelimit.contains(p.getName()))
			{
				playersignoringwelimit.remove(p.getName());
			}
			return false;
		}
	}
	
	public static int getPlotLimit(Player p, boolean regular)
	{
		int max = -2;
		int maxlimit = 10;
		
		if (regular){
			if (p.hasPermission("plots.limit.regular.*"))
			{
				return -1;
			}
			else
			{
				for(int ctr = 0; ctr < maxlimit; ctr++)
				{
					
					if(p.hasPermission("plots.limit.regular." + ctr))
					{
						max = ctr;
					}
				}
			}
		} else {
			if (p.hasPermission("plots.limit.donator.*"))
			{
				return -1;
			}
			else
			{
				for(int ctr = 0; ctr < maxlimit; ctr++)
				{
					
					if(p.hasPermission("plots.limit.donator." + ctr))
					{
						max = ctr;
					}
				}
			}
		}
		
		if(max == -2)
		{
			if(hasPerms(p, "plots.admin"))
				return -1;
			else
				return 0;
		}
		
		return max;
		
		
	}
	
	public static String getDate()
	{
		return getDate(Calendar.getInstance());
	}
	
	private static String getDate(Calendar cal)
	{
		int imonth = cal.get(Calendar.MONTH) + 1;
        int iday = cal.get(Calendar.DAY_OF_MONTH) + 1;
        String month = "";
        String day = "";
        
        if(imonth < 10)
        	month = "0" + imonth;
        else
        	month = "" + imonth;
        
        if(iday < 10)
        	day = "0" + iday;
        else
        	day = "" + iday;
        		
		return "" + cal.get(Calendar.YEAR) + "-" + month + "-" + day;
	}

	public static String getDate(java.sql.Date expireddate)
	{		
		return expireddate.toString();
	}
	
    private void setProtectedBlocks()
	{
		if (protectedblocks == null){
	    	protectedblocks = new ArrayList<Material>();
		} else {
			protectedblocks.clear();
		}
    	
		protectedblocks.add(Material.CHEST);
		protectedblocks.add(Material.FURNACE);
		protectedblocks.add(Material.BURNING_FURNACE);
		protectedblocks.add(Material.ENDER_PORTAL_FRAME);
		protectedblocks.add(Material.DIODE_BLOCK_ON);
		protectedblocks.add(Material.DIODE_BLOCK_OFF);
		protectedblocks.add(Material.JUKEBOX);
		protectedblocks.add(Material.NOTE_BLOCK);
		protectedblocks.add(Material.BED);
		protectedblocks.add(Material.CAULDRON);
		protectedblocks.add(Material.BREWING_STAND);
		protectedblocks.add(Material.BEACON);
		protectedblocks.add(Material.FLOWER_POT);
		protectedblocks.add(Material.ANVIL);
		protectedblocks.add(Material.DISPENSER);
		protectedblocks.add(Material.DROPPER);
		protectedblocks.add(Material.HOPPER);
		
	}
	
    private void setPreventedItems()
	{

    	if (preventeditems == null){
	    	preventeditems = new ArrayList<Material>();
		} else {
			preventeditems.clear();
		}
    	
		preventeditems.add(Material.FLINT_AND_STEEL);
		preventeditems.add(Material.MINECART);
		preventeditems.add(Material.POWERED_MINECART);
		preventeditems.add(Material.STORAGE_MINECART);
		preventeditems.add(Material.HOPPER_MINECART);
		preventeditems.add(Material.BOAT);
		
	}
	
	public void scheduleTask(Runnable task, int eachseconds, int howmanytimes)
	{		 		 
		PlotMod.cscurrentlyprocessingexpired.sendMessage("" + PlotMod.PLUGIN_PREFIX + ChatColor.RESET + caption("MsgStartDeleteSession"));
		
		for(int ctr = 0; ctr < (howmanytimes / nbperdeletionprocessingexpired); ctr++)
		{
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, task, ctr * eachseconds * 20);
		}
	}
	
	private void loadCaptions()
	{
		File filelang = new File(this.getDataFolder(), "caption-english.yml");
		
		TreeMap<String, String> properties = new TreeMap<String, String>();
		properties.put("MsgStartDeleteSession", PLUGIN_PREFIX + " Starting delete session"); //
		properties.put("MsgDeletedExpiredPlots", PLUGIN_PREFIX + " Deleted expired plot"); //
		properties.put("MsgDeleteSessionFinished", PLUGIN_PREFIX + " Deletion session finished, rerun to reset more plots."); //
		properties.put("MsgNotPlotWorld", Vars.Ntrl + "Oops. This" );
		properties.put("MsgPermissionDenied", Vars.Ntrl + "Oops. You don't have permission for this!"); //
		properties.put("MsgNoPlotFound", Vars.Ntrl + "Oops. A plot could not be found at your location!");
		properties.put("MsgCannotClaimRoad", Vars.Ntrl + "Oops. This is the road, you cannot claim here!");
		properties.put("MsgOwnedBy", "owned by");
		properties.put("MsgThisPlot", "This plot");
		properties.put("MsgThisPlotYours", "This plot is now yours.");
		properties.put("MsgThisPlotIsNow", "This plot is now ");
		properties.put("MsgThisPlotOwned", Vars.Ntrl + "Oops. This plot has already been claimed!");
		properties.put("MsgAlreadyReachedMaxPlots", "You have already reached your maximum amount of plots");
		properties.put("MsgToGetToIt", "to get to it");
		properties.put("MsgClaimedPlot", "claimed plot");
		properties.put("MsgRemovedPlot","removed the plot");
		properties.put("MsgPlotNoLongerProtected", NOTIFY_PREFIX + "This plot is " + Vars.Neg + "NO LONGER" + Vars.Ntrl + " protected. It may be cleared or unclaimed.");
		properties.put("MsgUnprotectedPlot", "unprotected plot");
		properties.put("MsgPlotNowProtected", NOTIFY_PREFIX + "This plot is now " + Vars.Pos + "PROTECTED" + Vars.Ntrl + ". It may not be cleared or unclaimed.");
		properties.put("MsgProtectedPlot", "protected plot");
		properties.put("MsgNoPlotsFinished", NOTIFY_PREFIX + "No plots are currently marked as done!");
		properties.put("MsgUnmarkFinished",NOTIFY_PREFIX + "This plot is " + Vars.Neg + "NO LONGER" + Vars.Ntrl + " marked as done.");
		properties.put("MsgMarkFinished", NOTIFY_PREFIX + "This plot is " + Vars.Pos + "NOW MARKED" + Vars.Ntrl + " as done.");
		properties.put("MsgPlotExpirationReset", NOTIFY_PREFIX + "The expiration date for this plot has been reset!");
		properties.put("MsgNoPlotExpired", NOTIFY_PREFIX + "There are no plots which are expired!");
		properties.put("MsgPlotProtectedCannotUnclaim", Vars.Ntrl + "Oops. The plot can't be unclaimed becuase it's protected!");
		properties.put("MsgPlotProtectedCannotClear", Vars.Ntrl + "Oops. The plot can't be cleared because it's protected!");
		properties.put("MsgPlotCleared",Vars.NOTICE_PREFIX + "The plot has been cleared.");
		properties.put("MsgClearedPlot","cleared plot");
		properties.put("MsgToPlot","to plot");
		properties.put("MsgFromPlot","from plot");
		properties.put("MsgNowOwnedBy","is now owned by");
		properties.put("MsgChangedOwnerFrom","changed owner from");
		properties.put("MsgChangedOwnerOf","changed owner of");
		properties.put("MsgOwnerChangedTo","Plot Owner has been set to");
		properties.put("MsgExchangedPlot","exchanged plot");
		properties.put("MsgAndPlot","and plot");
		properties.put("MsgReloadedSuccess", PLUGIN_PREFIX + "The plot software has been reloaded!");
		properties.put("MsgReloadedConfigurations", PLUGIN_PREFIX + "The configurations have been reloaded!");
		properties.put("MsgWorldNotPlot","does not exist or is not a plot world.");
		
		
		properties.put("HelpLimit", ChatColor.WHITE + "See how many claims you have and the limit");
		properties.put("HelpClaim", ChatColor.WHITE + "Will claim the plot you're in, if possible");
		properties.put("HelpClaimOther", ChatColor.WHITE + "Will claim the plot you're in for someone else, if possible");
		properties.put("HelpAutoClaim", ChatColor.WHITE + "Will find & claim a plot for you");
		properties.put("HelpHome", ChatColor.WHITE + "Teleports you to your (first) plot");
		properties.put("HelpHome2", ChatColor.WHITE + "Teleports you to one of your plots, if you have multiple");
		properties.put("HelpHomeOther", ChatColor.WHITE + "Teleports you to player's (first) plot");
		properties.put("HelpHomeOther2", ChatColor.WHITE + "Teleports you to one of player's plots, if they have multiple");
		properties.put("HelpScrutinize", ChatColor.WHITE + "Investigates the plot you're currently in");
		properties.put("HelpComment", ChatColor.WHITE + "Comment on the buildings of the plot you're in");
		properties.put("HelpComments", ChatColor.WHITE + "Get all of the current comments on the plot you're in");
		properties.put("HelpList", ChatColor.WHITE + "List all of the plots you can build on");
		properties.put("HelpListOther", ChatColor.WHITE + "List all of the plots PLAYER can build on");
		properties.put("HelpBiomeInfo", ChatColor.WHITE + "Displays the biome of the plot in which you stand");
		properties.put("HelpBiome", ChatColor.WHITE + "Sets the plot biome as the BIOME given");
		properties.put("HelpBiomeList", ChatColor.WHITE + "Gets a list of all possible biomes");
		properties.put("HelpDone", ChatColor.WHITE + "Marks your plot as either finished, or not finished");
		properties.put("HelpTp", ChatColor.WHITE + "Teleport to a plot");
		properties.put("HelpId", ChatColor.WHITE + "Gets the ID of the plot you're in");
		properties.put("HelpClear", ChatColor.WHITE + "Clears the plot of everything built thus far");
		properties.put("HelpFriend", ChatColor.WHITE + "Allows a player to build in your plot");
		properties.put("HelpFriendDisclaimer", Vars.Neg + "NOTICE:" + Vars.Ntrl + " TypicalCraft is not responsible for any griefs!");
		properties.put("HelpDeny", ChatColor.WHITE + "Blocks a player from entering your plot");
		properties.put("HelpUnfriend", ChatColor.WHITE + "Stops allowing a player to build in your plot");
		properties.put("HelpUndeny", ChatColor.WHITE + "Stops preventing a player from entering your plot");
		properties.put("HelpSetowner", ChatColor.WHITE + "Sets PLAYER as the owner of the plot in which you stand");
		properties.put("HelpMove", ChatColor.WHITE + "Swaps the blocks of a plot with another");
		properties.put("HelpExpired", ChatColor.WHITE + "Gets a list of expired plots");
		properties.put("HelpDoneList", ChatColor.WHITE + "Gets a list of plots which are marked as 'done'");
		properties.put("HelpAddTime", ChatColor.WHITE + "Pushes back the expiration date of a plot by the DAYS specified");
		properties.put("HelpReload", ChatColor.WHITE + "Reloads the plugin and config files");
		properties.put("HelpUnclaim", ChatColor.WHITE + "Unclaims the plot, ALL PROGRESS WILL BE LOST");
		properties.put("HelpResetExpired", ChatColor.WHITE + "Resets the 50 oldest plots of the world");
		
		properties.put("SignOwner", "Owner:");
		properties.put("SignId", "ID:");
		
		
		properties.put("CommandResetExpired", "resetexpired");
		properties.put("CommandHelp", "help");
		properties.put("CommandClaim", "claim");
		properties.put("CommandAutoClaim", "autoclaim");
		properties.put("CommandScrutinize", "scrutinize");
		properties.put("CommandComment", "comment");
		properties.put("CommandComments", "comments");
		properties.put("CommandBiome", "biome");
		properties.put("CommandBiomeList", "biomelist");
		properties.put("CommandId", "id");
		properties.put("CommandTp", "tp");
		properties.put("CommandClear", "clear");
		properties.put("CommandFriend", "friend");
		properties.put("CommandDeny", "deny");
		properties.put("CommandUnfriend", "unfriend");
		properties.put("CommandUndeny", "undeny");
		properties.put("CommandSetowner", "setowner");
		properties.put("CommandLimit", "limit");
		properties.put("CommandMove", "move");
		properties.put("CommandList", "list");
		properties.put("CommandExpired", "expired");
		properties.put("CommandAddtime", "addtime");
		properties.put("CommandDone", "done");
		properties.put("CommandDoneList", "donelist");
		properties.put("CommandProtect", "protect");
		properties.put("CommandUnclaim", "unclaim");
		properties.put("CommandHome", "home");
		
		properties.put("ErrCannotBuild", Vars.Ntrl + "Oops. You aren't allowed to build here!");
		properties.put("ErrCannotUseEggs", Vars.Ntrl + "Oops. You aren't allowed to use eggs here!");
		properties.put("ErrCannotUse", Vars.Ntrl + "Oops. You cannot use that!");
		properties.put("ErrCreatingPlotAt", "An error occured while creating the plot at");
		
		CreateConfig(filelang, properties, "Plots Caption configuration ????");
		
		if (language != "english")
		{
			filelang = new File(this.getDataFolder(), "caption-" + language + ".yml");
			CreateConfig(filelang, properties, "Plots Caption configuration");
		}
		
		InputStream input = null;
		
		try
		{				
			input = new FileInputStream(filelang);
		    Yaml yaml = new Yaml();
		    Object obj = yaml.load(input);
		
		    if(obj instanceof LinkedHashMap<?, ?>)
		    {
				@SuppressWarnings("unchecked")
				LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) obj;
							    
			    captions = new HashMap<String, String>();
				for(String key : data.keySet())
				{
					captions.put(key, data.get(key));
				}
		    }
		} catch (FileNotFoundException e) {
			logger.severe("[" + NAME + "] File not found: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.severe("[" + NAME + "] Error with configuration: " + e.getMessage());
			e.printStackTrace();
		} finally {                      
			if (input != null) try {
				input.close();
			} catch (IOException e) {}
		}
	}
	
	private void CreateConfig(File file, TreeMap<String, String> properties, String Title)
	{
		if(!file.exists())
		{
			BufferedWriter writer = null;
			
			try{
				File dir = new File(this.getDataFolder(), "");
				dir.mkdirs();			
				
				writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8"));
				writer.write("# " + Title + "\n");
				
				for(Entry<String, String> e : properties.entrySet())
				{
					writer.write(e.getKey() + ": '" + e.getValue().replace("'", "''") + "'\n");
				}
				
				writer.close();
			}catch (IOException e){
				logger.severe("[" + NAME + "] Unable to create config file : " + Title + "!");
				logger.severe(e.getMessage());
			} finally {                      
				if (writer != null) try {
					writer.close();
				} catch (IOException e2) {}
			}
		}
		else
		{
			OutputStreamWriter writer = null;
			InputStream input = null;
			
			try
			{				
				input = new FileInputStream(file);
			    Yaml yaml = new Yaml();
			    Object obj = yaml.load(input);
			    
			    if(obj instanceof LinkedHashMap<?, ?>)
			    {
					@SuppressWarnings("unchecked")
					LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) obj;
					
				    writer = new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8");
					
					for(Entry<String, String> e : properties.entrySet())
					{						
						if (!data.containsKey(e.getKey()))
							writer.write("\n" + e.getKey() + ": '" + e.getValue().replace("'", "''") + "'");
					}
					
					writer.close();
					input.close();
			    }
			} catch (FileNotFoundException e) {
				logger.severe("[" + NAME + "] File not found: " + e.getMessage());
				e.printStackTrace();
			} catch (Exception e) {
				logger.severe("[" + NAME + "] Error with configuration: " + e.getMessage());
				e.printStackTrace();
			} finally {                      
				if (writer != null) try {
					writer.close();
				} catch (IOException e2) {}
				if (input != null) try {
					input.close();
				} catch (IOException e) {}
			}
		}
	}
	
	public static String caption(String s)
	{
		if(captions.containsKey(s))
		{
			return addColor(captions.get(s));
		}else{
			logger.warning("[" + NAME + "] Missing caption: " + s);
			return "ERROR:Missing caption '" + s + "'";
		}
	}
	
	public static String addColor(String string) 
	{
		return ChatColor.translateAlternateColorCodes('&', string);
    }
	
	private short getBlockId(ConfigurationSection cs, String section, String def)
	{
		String idvalue = cs.getString(section, def.toString());
		if(idvalue.indexOf(":") > 0)
		{
			return Short.parseShort(idvalue.split(":")[0]);
		}
		else
		{
			return Short.parseShort(idvalue);
		}
	}
	
	private byte getBlockValue(ConfigurationSection cs, String section, String def)
	{
		String idvalue = cs.getString(section, def.toString());
		if(idvalue.indexOf(":") > 0)
		{
			return Byte.parseByte(idvalue.split(":")[1]);
		}
		else
		{
			return 0;
		}
	}
	
	private String getBlockIdValue(Short id, Byte value)
	{
		return (value == 0) ? id.toString() : id.toString() + ":" + value.toString();
	}
	
	public static boolean isChatThere(){
		
		if (Bukkit.getServer().getPluginManager().getPlugin("TypicalChat") != null){
			return true; 
		} else {return false;}		
	}
	
	public static boolean isChatEnabled(){
		
		if (Bukkit.getServer().getPluginManager().getPlugin("TypicalChat") == null){
			return false;
		}	
		
		return Bukkit.getServer().getPluginManager().isPluginEnabled("TypicalChat");	
	}
}
