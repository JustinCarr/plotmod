package org.jkjkkj.PlotMod;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import com.universicraft_mp.TypicalChat.Vars;

public class PMCommand implements CommandExecutor
{
	private PlotMod plugin;
	
	public PMCommand(PlotMod instance)
	{
		plugin = instance;
	}
	
	private String C(String caption)
	{
		return PlotMod.caption(caption);
	}
	
	public boolean onCommand(CommandSender s, Command c, String l, String[] args)
	{
		if(l.equalsIgnoreCase("plots") || l.equalsIgnoreCase("plot") || l.equalsIgnoreCase("p"))
		{
			if(!(s instanceof Player))
			{
				if(args.length == 0 || args[0].equalsIgnoreCase("1"))
				{
					s.sendMessage(Vars.Ntrl + Vars.CHAT_BAR_LINE); 
					s.sendMessage(ChatColor.GRAY + "CONSOLE HELP MESSAGE...");
					s.sendMessage(Vars.Ntrl + "/plots reload " + ChatColor.WHITE + " Reloads the plugin and config files");
					s.sendMessage(Vars.Ntrl + Vars.CHAT_BAR_LINE); 
					return true;
				}
				else
				{
					String a0 = args[0].toString();
					if (a0.equalsIgnoreCase("reload")) { return reload(s, args);}
					if (a0.equalsIgnoreCase(C("CommandResetExpired"))) { return resetexpired(s, args); }

					
				}
			}
			else
			{
				Player p = (Player) s;
				
				if(args.length == 0)
				{
					return showhelp(p, 1);
				}
				else
				{
					String a0 = args[0].toString();
					int ipage = -1;
					
					try  
					{  
						ipage = Integer.parseInt( a0 );
						if (ipage <= 0){
							ipage = -1;
						}
					}  
					catch( Exception e) {}
									
					if(ipage != -1)
					{
						return showhelp(p, ipage);
					}
					else
					{
						if (a0.equalsIgnoreCase(C("CommandHelp")))
						{
							ipage = -1;
							
							if(args.length > 1)
							{
								String a1 = args[1].toString();
								ipage = -1;
								
								try  
								{  
									int i = Integer.parseInt( a1 ); 
									
									if (i <= 0 ){
										ipage = -1;
									} else {
										ipage = i;
									}
									
								}  
								catch( Exception e) {
									p.sendMessage(Vars.Ntrl + "Oops. The page argument must be an integer.");
									p.sendMessage(Vars.Ntrl + "Try: " + Vars.Neg + "/plots help INTEGER_NUMBER");
									return true;
								}
							}
							
							if(ipage != -1)
							{
								return showhelp(p, ipage);
							}
							else
							{
								return showhelp(p, 1);
							}
						}
						if (a0.equalsIgnoreCase(C("CommandClaim"))) { return claim(p, args);}
						if (a0.equalsIgnoreCase(C("CommandAutoClaim"))) { return autoclaim(p, args);}
						if (a0.equalsIgnoreCase(C("CommandScrutinize")) || a0.equalsIgnoreCase("s")) { return scrutinize(p, args);}
						if (a0.equalsIgnoreCase(C("CommandComment"))) { return comment(p, args);}
						if (a0.equalsIgnoreCase(C("CommandComments")) || a0.equalsIgnoreCase("c")) { return comments(p, args);}
						if (a0.equalsIgnoreCase(C("CommandBiome")) || a0.equalsIgnoreCase("b")) { return biome(p, args);}
						if (a0.equalsIgnoreCase(C("CommandBiomelist"))) { return biomelist(p, args);}
						if (a0.equalsIgnoreCase(C("CommandId"))) { return id(p, args);}
						if (a0.equalsIgnoreCase(C("CommandTp")) || a0.equalsIgnoreCase("teleport")) { return tp(p, args);}
						if (a0.equalsIgnoreCase(C("CommandClear"))) { return clear(p, args);}
						if (a0.equalsIgnoreCase(C("CommandFriend"))) { return friend(p, args);}
						if(PlotMod.allowToDeny)
						{
							if (a0.equalsIgnoreCase(C("CommandDeny"))) { return deny(p, args);}
							if (a0.equalsIgnoreCase(C("CommandUndeny"))) { return undeny(p, args);}
						}
						if (a0.equalsIgnoreCase(C("CommandUnfriend"))) { return unfriend(p, args);}
						if (a0.equalsIgnoreCase(C("CommandSetowner")) || a0.equalsIgnoreCase("o")) { return setowner(p, args);}
						if (a0.equalsIgnoreCase(C("CommandMove")) || a0.equalsIgnoreCase("m")) { return move(p, args);}
						if (a0.equalsIgnoreCase("reload")) { return reload(s, args);}
						if (a0.equalsIgnoreCase(C("CommandList"))) { return plotlist(p, args);}
						if (a0.equalsIgnoreCase(C("CommandExpired"))) { return expired(p, args);}
						if (a0.equalsIgnoreCase(C("CommandAddtime"))) { return addtime(p, args);}
						if (a0.equalsIgnoreCase(C("CommandDone"))) { return done(p, args);}
						if (a0.equalsIgnoreCase(C("CommandDoneList"))) { return donelist(p, args);}
						if (a0.equalsIgnoreCase(C("CommandProtect"))) { return protect(p, args);}
						if (a0.equalsIgnoreCase(C("CommandLimit"))) { return this.showLimit(p);}
						if (a0.equalsIgnoreCase(C("CommandUnclaim"))) { return unclaim(p, args);}
						if (a0.startsWith(C("CommandHome"))) { return home(p, args);}
						if (a0.equalsIgnoreCase(C("CommandResetExpired"))) { return resetexpired(p, args); }
						p.sendMessage(Vars.Ntrl + "Oops. Incorrect Arguments. For help: " + Vars.Neg + "/plots help");
						return true;
					}
				}
			}
			return true;
		}
		return false;
	}
	
	private boolean resetexpired(CommandSender s, String[] args)
	{
		if(PlotMod.hasPerms(s, "plots.admin.resetexpired"))
		{
			if(args.length <= 1)
			{
				s.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/plots " + C("CommandResetExpired") + " WORLD_NAME");
			}
			else
			{
				if(PlotMod.worldcurrentlyprocessingexpired != null)
				{
					String nameofprocessing = ((PlotMod.cscurrentlyprocessingexpired instanceof Player) ? PlotMod.cscurrentlyprocessingexpired.getName() : "Console");
					
					s.sendMessage(PlotMod.PLUGIN_PREFIX + "Oops. Player " + Vars.Pos + nameofprocessing + Vars.Ntrl + " is already processing expired plots!");
				}
				else
				{
					World w = s.getServer().getWorld(args[1]);
					
					if(w == null)
					{
						s.sendMessage(Vars.Ntrl + "Oops. The world '" + args[1] + "' doesn't exist or isn't loaded!");
						return true;
					}
					else
					{					
						if(!PlotManager.isPlotWorld(w))
						{
							s.sendMessage(Vars.Ntrl + "Oops. The world '" + w.getName() + "' is not a plot world!");
							return true;
						}
						else
						{
							PlotMod.worldcurrentlyprocessingexpired = w;
							PlotMod.cscurrentlyprocessingexpired = s;
							PlotMod.counterexpired = 50;
							PlotMod.nbperdeletionprocessingexpired = 5;
							
							plugin.scheduleTask(new PlotRunnableDeleteExpire(), 5, 50);
						}
					}
				}
			}
		} 
		else
		{
			s.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}

	private boolean protect(Player p, String[] args) 
	{
		if(PlotMod.hasPerms(p, "plots.admin.protect") || PlotMod.hasPerms(p, "plots.use.protect"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");

				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

				return true;
			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
					p.sendMessage(Vars.Ntrl + "Oops. Protecting plots in competition isn't allowed!");
			}
			else
			{
				String id = PlotManager.getPlotId(p.getLocation());
				
				if(id.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					if(!PlotManager.isPlotAvailable(id, p))
					{
						Plot plot = PlotManager.getPlotById(p,id);
						
						String name = p.getName();
						
						if(plot.owner.equalsIgnoreCase(name) || PlotMod.hasPerms(p, "plots.admin.protect"))
						{
							if(plot.protect)
							{
								plot.protect = false;
								PlotManager.adjustWall(p.getLocation());
								
								plot.updateField("protected", false);
								p.sendMessage(C("MsgPlotNoLongerProtected"));
							}
							else
							{								
								
								plot.protect = true;
								PlotManager.adjustWall(p.getLocation());
								
								plot.updateField("protected", true);
								p.sendMessage(C("MsgPlotNowProtected"));
								
							}
						}
						else
						{
							p.sendMessage(Vars.Ntrl + "Oops. This plot is not yours, you aren't allowed to protect it! (ID: " + id + ")");
						}
					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. This plot does not have an owner! (ID: " + id + ")");
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}

	private boolean donelist(Player p, String[] args) 
	{
		if(PlotMod.hasPerms(p, "plots.admin.done"))
		{
			PlotMapInfo pmi = PlotManager.getMap(p);
			
			if(pmi == null)
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

				return true;
			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. This feature only applies to Donator and Regular worlds!");
			}
			else
			{
				
				HashMap<String, Plot> plots = pmi.plots;
				List<Plot> finishedplots = new ArrayList<Plot>();
				int nbfinished = 0;
				int maxpage = 0;
				int pagesize = 8;
				int page = 1;
				
				if(args.length == 2)
				{
					try
					{
						page = Integer.parseInt(args[1]);
					}catch(NumberFormatException ex){}
				}
				
				for(String id : plots.keySet())
				{
					Plot plot = plots.get(id);
					
					if(plot.finished)
					{
						finishedplots.add(plot);
						nbfinished++;
					}
				}
				
				Collections.sort(finishedplots, new PlotFinishedComparator());
				
				maxpage = (int) Math.ceil(((double)nbfinished/(double)pagesize));
				
				if(finishedplots.size() == 0)
				{
					
					p.sendMessage(C("MsgNoPlotsFinished"));
				}
				else
				{
					String a = ChatColor.AQUA + "";
					String g = ChatColor.GOLD + "";
					String w = ChatColor.WHITE + "";
					p.sendMessage(a + "======" + g + "[Finished Plots (" + a + page + g + "/" + a + maxpage + g + ")]" + a + "======");
					
					for(int i = (page-1) * pagesize; i < finishedplots.size() && i < (page * pagesize); i++)
					{	
						Plot plot = finishedplots.get(i);
						
						String text = g + "- " + w + plot.owner +g + " on " +w+ plot.finisheddate +g+ " (ID: " + w + plot.id + g + ")";
												
						p.sendMessage(text);
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}

	private boolean done(Player p, String[] args)
	{
		if(PlotMod.hasPerms(p, "plots.use.done") || PlotMod.hasPerms(p, "plots.admin.done"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
				return true;
			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. This only applies to Donator and Regular worlds!");
			}
			else
			{
				String id = PlotManager.getPlotId(p.getLocation());
				
				if(id.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					if(!PlotManager.isPlotAvailable(id, p))
					{
						Plot plot = PlotManager.getPlotById(p,id);
						String name = p.getName();
						
						if(plot.owner.equalsIgnoreCase(name) || PlotMod.hasPerms(p, "plots.admin.done"))
						{							
							if(plot.finished)
							{
								plot.setUnfinished();
								p.sendMessage(C("MsgUnmarkFinished"));
								
							}
							else
							{
								plot.setFinished();
								p.sendMessage(C("MsgMarkFinished"));
								
							}
						}
					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. This plot does not have an owner! (ID: " + id + ")");
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean addtime(Player p, String[] args)
	{
		if(PlotMod.hasPerms(p, "plots.admin.addtime"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

				return true;
			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. Plots in Competition don't expire anyways!");
			}
			else
			{
				String id = PlotManager.getPlotId(p.getLocation());
				
				if(id.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					if(!PlotManager.isPlotAvailable(id, p))
					{
						Plot plot = PlotManager.getPlotById(p,id);
						
						if(plot != null)
						{
							
							plot.resetExpire(PlotManager.getMap(p).DaysToExpiration);
							p.sendMessage(C("MsgPlotExpirationReset"));
							
						}
					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. This plot does not have an owner! (ID: " + id + ")");
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}

	private boolean expired(Player p, String[] args)
	{
		if(PlotMod.hasPerms(p, "plots.admin.expired"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

				return true;
			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. Plots in Competition don't expire!");
			}
			else
			{
				int pagesize = 8;
				int page = 1;
				int maxpage = 0;
				int nbexpiredplots = 0; 
				World w = p.getWorld();
				List<Plot> expiredplots = new ArrayList<Plot>();
				HashMap<String, Plot> plots = PlotManager.getPlots(w);
				String date = PlotMod.getDate();
				
				if(args.length == 2)
				{
					try
					{
						page = Integer.parseInt(args[1]);
					}catch(NumberFormatException ex){}
				}
				
				for(String id : plots.keySet())
				{
					Plot plot = plots.get(id);
					
					if(!plot.protect && plot.expireddate != null && PlotMod.getDate(plot.expireddate).compareTo(date.toString()) < 0)
					{
						nbexpiredplots++;
						expiredplots.add(plot);
					}
				}
				
				Collections.sort(expiredplots);
								
				maxpage = (int) Math.ceil(((double)nbexpiredplots/(double)pagesize));
				
				if(expiredplots.size() == 0)
				{
					p.sendMessage(C("MsgNoPlotExpired"));
				}
				else
				{
					
					String a = ChatColor.AQUA + "";
					String g = ChatColor.GOLD + "";
					String wh = ChatColor.WHITE + "";
					p.sendMessage(a + "======" + g + "[Expired Plots (" + a + page + g + "/" + a + maxpage + g + ")]" + a + "======");

					for(int i = (page-1) * pagesize; i < expiredplots.size() && i < (page * pagesize); i++)
					{	
						Plot plot = expiredplots.get(i);
						
						String text = Vars.Ntrl + "- " + wh + plot.owner + Vars.Ntrl + " on " +wh+ plot.expireddate.toString() + Vars.Ntrl + " (ID: " + wh + plot.id + Vars.Ntrl + ")";
						p.sendMessage(text);
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}

	
    private boolean plotlist(Player p, String[] args)
	{
		if(PlotMod.hasPerms(p, "plots.use.list"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

				return true;
			}
			else
			{
			    String name;
			    String pname = p.getName();
				
				if(PlotMod.hasPerms(p, "plots.admin.list") && args.length == 2)
				{
				    name = args[1];
				    p.sendMessage(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
				    p.sendMessage(ChatColor.GOLD + "Here is a list of plots where '" + name + "' can build in...");
				}
				else
				{
				    name = p.getName();
				    p.sendMessage(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
				    p.sendMessage(ChatColor.GOLD + "Here is a list of plots where you can build in...");
				}
				
				String g = ChatColor.GOLD + "";
				String w = ChatColor.WHITE + "";
				for(Plot plot : PlotManager.getPlots(p).values())
				{
					StringBuilder addition = new StringBuilder();
						
					if(plot.expireddate != null)
					{
						java.util.Date tempdate = plot.expireddate; 
						
						if(tempdate.compareTo(Calendar.getInstance().getTime()) < 0)
						{
							addition.append(Vars.Ntrl + "Expired: " + w + plot.expireddate.toString());
						}else{
							addition.append(g + "Expires: " + w + plot.expireddate.toString());
						}
					}
						
					if(plot.owner.equalsIgnoreCase(name))
					{
						if(plot.allowedcount() == 0)
						{
							if(name.equalsIgnoreCase(pname))
								p.sendMessage(g + "- " + w + ChatColor.ITALIC + "YOURS" + g + " (ID: " +w+plot.id+g+ ") " + addition);
							else
								p.sendMessage(g + "- " + w + plot.owner + g + " (ID: " +w+plot.id+g+ ") " + addition);
						}
						else
						{
							StringBuilder helpers = new StringBuilder();
							
							Object[] thosewhoareallowed = plot.allowed().toArray();
							String comma = "";
							for(int i = 0 ; i < plot.allowedcount(); i++)
							{
								helpers.append(comma).append(w + thosewhoareallowed[i]);
								comma = g+", ";
							}
							
							if(name.equalsIgnoreCase(pname))
								p.sendMessage(g + "- " + w + ChatColor.ITALIC + "YOURS" + g + " (ID: " +w+plot.id+g+ ") " + addition +g+ " Friends: " + w + helpers.toString());
							else
								p.sendMessage(g + "- " + w + plot.owner + g + " (ID: " +w+plot.id+g+ ") " + addition +g+ " Friends: " + w + helpers.toString());
						}
					}
					else if(plot.isAllowedConsulting(name))
					{
						StringBuilder helpers = new StringBuilder();
						String[] thosewhoareallowed = (String[]) plot.allowed().toArray();
						String comma = "";
						for(int i = 0 ; i < plot.allowedcount(); i++)
						{
							if(p.getName().equalsIgnoreCase(thosewhoareallowed[i]))
								if(name.equalsIgnoreCase(pname)) {
									helpers.append(comma).append(w + ChatColor.ITALIC + "You");
								} else {
									helpers.append(comma).append(w + args[1]);
								}
							else
								helpers.append(comma).append(w + thosewhoareallowed[i]);
							
							comma = g + ", ";
						
						}
						
						if(plot.owner.equalsIgnoreCase(pname))
							p.sendMessage(g + "- " + w + ChatColor.ITALIC + "YOURS" + g + " (ID: " +w+plot.id+g+ ") " + addition + g + " Friends: " + helpers.toString());
						else
							p.sendMessage(g + "- " + w + plot.owner + g + " (ID: " +w+plot.id+g+ ") " + addition + g + " Friends: " + helpers.toString());
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
    
    private boolean showLimit(Player p){
    	
    	if (PlotMod.hasPerms(p, "plots.use.limit")){
    		
    		
    		int totalregular = PlotManager.getNbOwnedPlot(p.getUniqueId(), true);
    		int totaldonator = PlotManager.getNbOwnedPlot(p.getUniqueId(), false);
    		
    		int maxregularplots = PlotMod.getPlotLimit(p, true);
    		int maxdonatorplots = PlotMod.getPlotLimit(p, false);
    			
    		ArrayList<String> tosend = new ArrayList<String>();
    		
    		tosend.add(Vars.NOTICE_PREFIX + "You own " + Vars.Pos + totalregular + Vars.Ntrl + " regular plots and " + Vars.Pos + totaldonator + Vars.Ntrl + " donator plots!");
    		tosend.add(Vars.NOTICE_PREFIX + "Max amount of regular plots you can own: " + Vars.Pos + (maxregularplots == -1 ? "INFINITY- NO LIMT": maxregularplots));
    		tosend.add(Vars.NOTICE_PREFIX + "Max amount of donator plots you can own: " + Vars.Pos + (maxdonatorplots == -1 ? "INFINITY- NO LIMT": maxdonatorplots));
    		
    		for (String s : tosend){
    			p.sendMessage(s);
    		}
    		
    	} 
    	else 
    	{	
    		p.sendMessage(C("MsgPermissionDenied"));	
    	}
    	return true;
    }
	
	private boolean showhelp(Player p, int page)
	{
		int max = 6;
		int maxpage = 0;
		
		List<String> allowed_commands = new ArrayList<String>();
		
		allowed_commands.add("help");
		if(PlotMod.hasPerms(p, "plots.use.limit")) allowed_commands.add("limit");
		if(PlotMod.hasPerms(p, "plots.use.claim")) allowed_commands.add("claim");
		if(PlotMod.hasPerms(p, "plots.use.claim.other")) allowed_commands.add("claim.other");
		if(PlotMod.hasPerms(p, "plots.use.autoclaim")) allowed_commands.add("autoclaim");
		if(PlotMod.hasPerms(p, "plots.use.home")) 
		{
			allowed_commands.add("home");
			allowed_commands.add("home2");
		}
		if(PlotMod.hasPerms(p, "plots.use.home.other"))
		{
			allowed_commands.add("home.other");
			allowed_commands.add("home.other2");
		}
		if(PlotMod.hasPerms(p, "plots.use.scrutinize"))
		{
			allowed_commands.add("scrutinize");
			allowed_commands.add("biomeinfo");
		}
		if(PlotMod.hasPerms(p, "plots.use.comment"))
		{
			allowed_commands.add("comment");
			allowed_commands.add("comment removeall");
		}
		if(PlotMod.hasPerms(p, "plots.use.comments")) allowed_commands.add("comments");
		if(PlotMod.hasPerms(p, "plots.use.list")) allowed_commands.add("list");
		if(PlotMod.hasPerms(p, "plots.use.biome"))
		{
			allowed_commands.add("biome");
			allowed_commands.add("biomelist");
		}
		if(PlotMod.hasPerms(p, "plots.use.done") || 
				PlotMod.hasPerms(p, "plots.admin.done")) allowed_commands.add("done");
		if(PlotMod.hasPerms(p, "plots.admin.done")) allowed_commands.add("donelist");
		if(PlotMod.hasPerms(p, "plots.admin.tp")) allowed_commands.add("tp");
		if(PlotMod.hasPerms(p, "plots.admin.id")) allowed_commands.add("id");
		if(PlotMod.hasPerms(p, "plots.use.clear") || 
				PlotMod.hasPerms(p, "plots.admin.clear")) allowed_commands.add("clear");
		if(PlotMod.hasPerms(p, "plots.use.unclaim") || 
				PlotMod.hasPerms(p, "plots.admin.unclaim")) allowed_commands.add("unclaim");
		if(PlotMod.hasPerms(p, "plots.use.friend") || 
				PlotMod.hasPerms(p, "plots.admin.friend")) allowed_commands.add("friend");
		if(PlotMod.hasPerms(p, "plots.use.unfriend") || 
				PlotMod.hasPerms(p, "plots.admin.unfriend")) allowed_commands.add("unfriend");
		if(PlotMod.allowToDeny)
		{
			if(PlotMod.hasPerms(p, "plots.use.deny") || 
					PlotMod.hasPerms(p, "plots.admin.deny")) allowed_commands.add("deny");
			if(PlotMod.hasPerms(p, "plots.use.undeny") || 
					PlotMod.hasPerms(p, "plots.admin.undeny")) allowed_commands.add("undeny");
		}
		if(PlotMod.hasPerms(p, "plots.admin.setowner")) allowed_commands.add("setowner");
		if(PlotMod.hasPerms(p, "plots.admin.move")) allowed_commands.add("move");
		if(PlotMod.hasPerms(p, "plots.admin.reload")) allowed_commands.add("reload");
		if(PlotMod.hasPerms(p, "plots.admin.list")) allowed_commands.add("listother");
		if(PlotMod.hasPerms(p, "plots.admin.expired")) allowed_commands.add("expired");
		if(PlotMod.hasPerms(p, "plots.admin.addtime")) allowed_commands.add("addtime");
		if(PlotMod.hasPerms(p, "plots.admin.resetexpired")) allowed_commands.add("resetexpired");
				
		maxpage = (int) Math.ceil((double) allowed_commands.size() / max);
		if (page > max){page = 1;}
		
		String gr = ChatColor.GRAY + "";
		String w = ChatColor.WHITE + "";
		String g = ChatColor.GOLD + "";
		
		p.sendMessage(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
		p.sendMessage(gr + "Help for the /plot command. Page: " + g + "(" + gr + page + g + "/" + gr + maxpage + g + ")");
		p.sendMessage(gr + "NOTICE: You can replace " + g + "/..." + gr + " with '" + g + "/plots" + gr + "', '" + g +"/plot" + gr +"' or '" + g + "/p" + gr +"'" );
		
		for(int ctr = (page - 1) * max; ctr < (page * max) && ctr < allowed_commands.size(); ctr++)
		{
			String allowedcmd = allowed_commands.get(ctr);
			if (allowedcmd.equalsIgnoreCase("help")){
				p.sendMessage(g + "/... help PAGE" + w + "View pages of the help");
			}
			else if(allowedcmd.equalsIgnoreCase("limit"))
			{
				p.sendMessage(g + "/... " + C("CommandLimit") + " " + C("HelpLimit"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("claim"))
			{
				p.sendMessage(g + "/... " + C("CommandClaim") + " " + C("HelpClaim"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("claim.other"))
			{
				p.sendMessage(g + "/... " + C("CommandClaim") + " PLAYER " + C("HelpClaimOther"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("autoclaim"))
			{
				p.sendMessage(g + "/... " + C("CommandAutoClaim") + " " + C("HelpAutoClaim"));				

			}
			else if(allowedcmd.equalsIgnoreCase("home"))
			{
				p.sendMessage(g + "/... " + C("CommandHome") + " " + C("HelpHome"));
				 
			}
			else if(allowedcmd.equalsIgnoreCase("home2"))
			{
				p.sendMessage(g + "/... " + C("CommandHome") + "#" + C("HelpHome2"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("home.other"))
			{
				p.sendMessage(g + "/... " + C("CommandHome") + " PLAYER " + C("HelpHomeOther"));
				
			}
			else if (allowedcmd.equalsIgnoreCase("home.other2"))
			{
				p.sendMessage(g + "/... " + C("CommandHome") + "# PLAYER " + C("HelpHomeOther2"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("scrutinize"))
			{
				p.sendMessage(g + "/... " + C("CommandScrutinize") + " " + C("HelpScrutinize"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("comment"))
			{
				p.sendMessage(g + "/... " + C("CommandComment") + " " + C("HelpComment"));

			}
			else if(allowedcmd.equalsIgnoreCase("comment removeall"))
			{
				p.sendMessage(g + "/... " + C("CommandComment") + " removeall " + ChatColor.WHITE + "Remove all comments you've made on a plot");
			}
			else if(allowedcmd.equalsIgnoreCase("comments"))
			{
				p.sendMessage(g + "/... " + C("CommandComments") + " " + C("HelpComments"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("list"))
			{
				p.sendMessage(g + "/... " + C("CommandList") + " " + C("HelpList"));
			}
			else if(allowedcmd.equalsIgnoreCase("listother"))
			{
				p.sendMessage(g + "/... " + C("CommandList") + " PLAYER " + C("HelpListOther"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("biomeinfo"))
			{
				p.sendMessage(g + "/... " + C("CommandBiome") + " " + C("HelpBiomeInfo"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("biome"))
			{
				p.sendMessage(g + "/... " + C("CommandBiome") + " BIOME " + C("HelpBiome"));

			}
			else if(allowedcmd.equalsIgnoreCase("biomelist"))
			{
				p.sendMessage(g + "/... " + C("CommandBiomeList") + " " + C("HelpBiomeList"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("done"))
			{
				p.sendMessage(g + "/... " + C("CommandDone") + " " + C("HelpDone"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("tp"))
			{
				p.sendMessage(g + "/... " + C("CommandTp") + " ID " + C("HelpTp"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("id"))
			{
				p.sendMessage(g + "/... " + C("CommandId") + " " + C("HelpId"));

			}
			else if(allowedcmd.equalsIgnoreCase("clear"))
			{
				p.sendMessage(g + "/... " + C("CommandClear") + " " + C("HelpClear"));

			}
			else if(allowedcmd.equalsIgnoreCase("friend"))
			{
				p.sendMessage(g + "/... " + C("CommandFriend") + " PLAYER " + C("HelpFriend"));
				p.sendMessage(C("HelpFriendDisclaimer"));

			}
			else if(allowedcmd.equalsIgnoreCase("deny"))
			{
				p.sendMessage(g + "/... " + C("CommandDeny") + " PLAYER " + C("HelpDeny"));

			}
			else if(allowedcmd.equalsIgnoreCase("unfriend"))
			{
				p.sendMessage(g + "/... " + C("CommandUnfriend") + " PLAYER " + C("HelpUnfriend"));

			}
			else if(allowedcmd.equalsIgnoreCase("undeny")){
				
				p.sendMessage(g + "/... " + C("CommandUndeny") + " PLAYER " + C("HelpUndeny"));

			}
			else if(allowedcmd.equalsIgnoreCase("setowner"))
			{
				p.sendMessage(g + "/... " + C("CommandSetowner") + " PLAYER " + C("HelpSetowner"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("move"))
			{
				p.sendMessage(g + "/... " + C("CommandMove") + " FROM(ID) TO(ID) " + C("HelpMove"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("expired"))
			{
				p.sendMessage(g + "/... " + C("CommandExpired") + " PAGE " + C("HelpExpired"));

			}
			else if(allowedcmd.equalsIgnoreCase("donelist"))
			{
				p.sendMessage(g + "/... " + C("CommandDoneList") + " PAGE " + C("HelpDoneList"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("addtime"))
			{
				p.sendMessage(g + "/... " + C("CommandAddtime") + " DAYS " + C("HelpAddTime"));

			}
			else if(allowedcmd.equalsIgnoreCase("reload"))
			{
				p.sendMessage(g + "/... reload " + C("HelpReload"));
				
			}
			else if(allowedcmd.equalsIgnoreCase("unclaim"))
			{
				p.sendMessage("/... " + C("CommandUnclaim") + " " + C("HelpUnclaim"));

			}
			else if(allowedcmd.equalsIgnoreCase("resetexpired"))
			{
				p.sendMessage(g + "/... " + C("CommandResetExpired") + " WORLD " + C("HelpResetExpired"));
			
			}
		}
		p.sendMessage(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
		
		return true;
	}
	
	private boolean tp(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.admin.tp"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

			}
			else
			{
				if(args.length == 2)
				{
					String id = args[1];
					
					if(!PlotManager.isValidId(id))
					{
						p.sendMessage(Vars.Ntrl + "Oops. That is not a valid ID. Proper example: " + Vars.Ntrl + "1;1");
						p.sendMessage(Vars.Ntrl + "Try: " + Vars.Neg + "/plots " + C("CommandTp") + " ID " );
						return true;
					}
					else
					{
						World w = p.getWorld();
						
						if(w == null || !PlotManager.isPlotWorld(w))
						{
							p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
							p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
						}
						else
						{
							Location bottom = PlotManager.getPlotBottomLoc(w, id);
							Location top = PlotManager.getPlotTopLoc(w, id);
							
							p.teleport(new Location(w, bottom.getX() + (top.getBlockX() - bottom.getBlockX())/2, PlotManager.getMap(w).RoadHeight + 2, bottom.getZ() - 2));
							p.sendMessage(Vars.NOTICE_PREFIX + "You're now at the plot marked by ID: " + Vars.Pos + id);
						}
					}
				}
				else
				{
					p.sendMessage(Vars.Ntrl + "Oops. Incorrect usage. Try: " + Vars.Neg + "/plots " + C("CommandTp") + " ID");
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}

	private boolean autoclaim(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.use.autoclaim"))
		{			
			if(!PlotManager.isPlotWorld(p))
			{
				
				if (PlotMod.hasPerms(p, "creative.donor")){
					p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
					p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

					return true;
				} else {
					
					if (PlotManager.isPlotWorld(Vars.regular_plotworld)){
						if(!sendToRegularWorld(p, false)){
							return true;
						}
					} else {
						PlotMod.logger.severe(PlotMod.PLUGIN_PREFIX + "The Regular plot world " + Vars.regular_plotworld + " is not a PlotMod world!");
						p.sendMessage(Vars.Ntrl + "Oops. Something went wrong getting you to the regular world.");
						p.sendMessage(Vars.Ntrl + "PLEASE NOTIFY STAFF IMMEDIATELY!");
						return true;
					}
					
				}

			}
			else if (p.getWorld().getName().equalsIgnoreCase(Vars.donor_plotworld))
			{
				if (!PlotMod.hasPerms(p,  "creative.donor")){
					p.sendMessage(Vars.NOTICE_PREFIX + "Only donors may claim in this world!");
					return true;
				}
			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. You cannot autoclaim in the Competition world!");
				return true;
			}

			World w = p.getWorld();
			boolean regular = true;
			
			if (w.getName().equals(Vars.regular_plotworld)){
				regular = true;
			} else if (w.getName().equals(Vars.donor_plotworld)){
				regular = false;
			} else {
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");
				return true;
			}
			int plotlimit = PlotMod.getPlotLimit(p, regular);
			if((plotlimit != -1) && (PlotManager.getNbOwnedPlot(p.getUniqueId(), regular) >= plotlimit) && (!PlotMod.hasPerms(p, "plots.admin")))
			{
				p.sendMessage(Vars.Ntrl + "Oops. You've claimed the " + Vars.Neg + "MAX" + Vars.Ntrl + " amount of " + (regular ? "regular" : "donator") + " plots allowed!");
				p.sendMessage(Vars.Ntrl + "Your limit: " + Vars.Neg + plotlimit +Vars.Ntrl+ " Current total: " + Vars.Neg + PlotManager.getNbOwnedPlot(p.getUniqueId(), regular));
				return true;
			}
			else
			{
				PlotMapInfo pmi = PlotManager.getMap(w);
				int limit = pmi.PlotAutoLimit;
				
				for(int i = 0; i < limit; i++)
				{
					for(int x = -i; x <= i; x++)
					{
						for(int z = -i; z <= i; z++)
						{
							String id = "" + x + ";" + z;
							
							if(PlotManager.isPlotAvailable(id, w))
							{									
								String name = p.getName();
								UUID uuid = p.getUniqueId();
								
								Plot plot = PlotManager.createPlot(w, id, name, uuid);
								
								//PlotManager.adjustLinkedPlots(id, w);
								
								p.teleport(new Location(w, PlotManager.bottomX(plot.id, w) + (PlotManager.topX(plot.id, w) - 
										PlotManager.bottomX(plot.id, w))/2, pmi.RoadHeight + 2, PlotManager.bottomZ(plot.id, w) - 2));
	
								p.sendMessage(Vars.NOTICE_PREFIX + "This plot is now " + Vars.Pos + "YOURS" + Vars.Ntrl + ".");
								p.sendMessage(Vars.NOTICE_PREFIX + "To teleport back to it... " + Vars.Pos + "/plots " + C("CommandHome"));

								
								return true;
							}
						}
					}
				}
			
				p.sendMessage(Vars.Ntrl + "Oops. Within an area of " + (limit^2) + ", no plots were available. Sorry!");
			}
				
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}

	private boolean claim(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.use.claim") || PlotMod.hasPerms(p, "plots.admin.claim.other"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
				return true;

			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. You cannot claim manually in the Competition world!");
				return true;
			}
			else if (p.getWorld().getName().equalsIgnoreCase(Vars.donor_plotworld))
			{
				if (!PlotMod.hasPerms(p,  "creative.donor")){
					p.sendMessage(Vars.NOTICE_PREFIX + "Only donors may claim in this world!");
					return true;
				}
			}
			
			String id = PlotManager.getPlotId(p.getLocation());
			
			if(id.equals(""))
			{
				
				p.sendMessage(C("MsgCannotClaimRoad"));
			}
			else if(!PlotManager.isPlotAvailable(id, p))
			{
				p.sendMessage(C("MsgThisPlotOwned"));
			}
			else
			{
				String playername = p.getName();
				UUID uuid = p.getUniqueId();
				
				if(args.length == 2)
				{
					if(PlotMod.hasPerms(p, "plots.admin.claim.other"))
					{
						playername = args[1];
						uuid = null;
					}
				}
				
				
				World w = p.getWorld();
				boolean regular = true;
				
				if (playername.equals(p.getName())){
					if (w.getName().equals(Vars.regular_plotworld)){
						regular = true;
					} else if (w.getName().equals(Vars.donor_plotworld)){
						regular = false;
					} else {
						p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
						p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");
						return true;
					}
				}
				
				int plotlimit = PlotMod.getPlotLimit(p, regular);
				
				
				if(playername.equals(p.getName()) && (plotlimit != -1) && (PlotManager.getNbOwnedPlot(p.getUniqueId(), regular) >= plotlimit) && (!p.hasPermission("plots.admin")))
				{
					p.sendMessage(Vars.Ntrl + "Oops. You've claimed the " + Vars.Neg + "MAX" + Vars.Ntrl + " amount of " + (regular ? "regular" : "donator") + " plots allowed!");
					p.sendMessage(Vars.Ntrl + "Your limit: " + Vars.Neg + plotlimit +Vars.Ntrl+ " Current total: " + Vars.Neg + PlotManager.getNbOwnedPlot(p.getUniqueId(), regular));
					return true;
				}
				else
				{
					
					Plot plot = PlotManager.createPlot(w, id, playername, uuid);
					
					//PlotManager.adjustLinkedPlots(id, w);
	
					if(plot == null)
						p.sendMessage(Vars.Ntrl + "Oops. An accident happened while claiming the plot. (ID: " + id + ")");
					else
					{
						if(playername.equalsIgnoreCase(p.getName()))
						{
							p.sendMessage(Vars.NOTICE_PREFIX + "This plot is now " + Vars.Pos + "YOURS" + Vars.Ntrl + ".");
							p.sendMessage(Vars.NOTICE_PREFIX + "To teleport back to it... " + Vars.Pos + "/plots " + C("CommandHome"));
						}else{
							p.sendMessage(Vars.NOTICE_PREFIX + "This plot is now " + Vars.Pos + playername + "'s" + Vars.Ntrl + ".");
						}
						
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean home(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.use.home") || PlotMod.hasPerms(p, "plots.admin.home.other"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
			}
			else
			{
				boolean found = false;
				String playername = p.getName();
				UUID uuid = p.getUniqueId();
				int nb = 1;
				World w = null;
				
				w = p.getWorld();
				
				String homenum = null;
				
				if (args[0].startsWith("home") && args[0].length() > 4){
					
					homenum = args[0].substring(4);
					
				}
				
				if(homenum != null)
				{
					try{
						nb = Integer.parseInt(homenum);
						
						if (nb == 0){
							p.sendMessage(Vars.Ntrl + "Oops. The # cannot be zero!");
							p.sendMessage(Vars.Ntrl + "EXAMPLE: " + Vars.Neg + "/plots " + C("CommandHome") + "2");
							return true;
						} else if (nb < 0 ){
							p.sendMessage(Vars.Ntrl + "Oops. The # cannot be negative!");
							p.sendMessage(Vars.Ntrl + "EXAMPLE: " + Vars.Neg + "/plots " + C("CommandHome") + "2");
							return true;
						}
						
					}catch(NumberFormatException ex)
					{
						p.sendMessage(Vars.Ntrl + "Oops. The # must be an integer value.");
						p.sendMessage(Vars.Ntrl + "EXAMPLE: " + Vars.Neg + "/plots " + C("CommandHome") + "2");
						return true;
					}
				}
				
				if(args.length >= 2)
				{
					
					if(PlotMod.hasPerms(p, "plots.admin.home.other"))
					{
						playername = args[1];
						uuid = null;
					}
					
				}
				
				int i = nb - 1;
				
				for(Plot plot : PlotManager.getPlots(w).values())
				{
					if(uuid == null && plot.owner.equalsIgnoreCase(playername) || uuid != null && plot.ownerId != null && plot.ownerId.equals(uuid))
					{
						if(i == 0)
						{							
							p.teleport(PlotManager.getPlotHome(w, plot));
							
							return true;
						}else{
							i--;
						}
					}
				}
				
				if(!found)
				{
					if(nb > 0 && nb != 1)
					{
						if(!playername.equalsIgnoreCase(p.getName()))
						{
							p.sendMessage(Vars.Ntrl + "Oops. '" + playername + "' doesn't have plot number " + nb + " in this world!");
						}else{
							p.sendMessage(Vars.Ntrl + "Oops. You don't have plot number " + nb + " in this world!");
						}
					}
					else if(!playername.equalsIgnoreCase(p.getName()))
					{
						p.sendMessage(Vars.Ntrl + "Oops. '" + playername + "' does not own a plot in this world!");
					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. Couldn't find home- you don't have a plot in this world!");
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean scrutinize(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.use.scrutinize"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

			}
			else
			{
				String id = PlotManager.getPlotId(p.getLocation());
				
				if(id.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					if(!PlotManager.isPlotAvailable(id, p))
					{
						Plot plot = PlotManager.getPlotById(p,id);
						
						ArrayList<String> tosend = new ArrayList<String>();
						String g = ChatColor.GOLD + "";
						String w = ChatColor.WHITE + "";
						
						tosend.add(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
						tosend.add(g + "I looked up some info about this plot for you... (ID: " + id + ")");
						tosend.add(g + "Owner: " + w + plot.owner);
						tosend.add(g + "Biome: " + w + FormatBiome(plot.biome.name(), false));
						tosend.add(g + "Expires: " + ((plot.expireddate == null) ? (Vars.Neg + "NEVER") : w + (plot.expireddate.toString())));
						tosend.add(g + "Finished: " + ((plot.finished) ? (Vars.Pos + "YES") : (Vars.Neg + "NO")) + g + " Protected: " + ((plot.protect) ? (Vars.Pos + "YES") : (Vars.Neg + "NO")));
						if (plot.allowedcount() > 0)
						{
							tosend.add(g + "Friends: " + w + plot.getAllowed());
						}
						else
						{
							tosend.add(g + "Friends: " + Vars.Neg + "NONE");
						}
						if(PlotMod.allowToDeny && plot.deniedcount() > 0)
						{
							tosend.add(g + "Denied: " + w + plot.getDenied());
						}
						else
						{
							tosend.add(g + "Denied: " + Vars.Neg + "NONE");
						}
						tosend.add(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
						for (String s : tosend){
							p.sendMessage(s);
						}
						
						
					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. This plot is not owned! ID: (" + id + ")");
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean comment(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.use.comment"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. Comments aren't enabled in the Competition world!");
				return true;
			}
			else
			{
				if(args.length < 2)
				{
					p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/plots " + C("CommandComment") + " COMMENT");

				}
				else
				{
					String id = PlotManager.getPlotId(p.getLocation());
					
					if(id.equals(""))
					{
						p.sendMessage(C("MsgNoPlotFound"));
					}
					else
					{
						if(!PlotManager.isPlotAvailable(id, p))
						{
							
							if (args[1].equalsIgnoreCase("removeall")){
								
								p.sendMessage(Vars.Ntrl + "Oops. This feature hasn't been implemented yet!");
								
							} else {
								String playername = p.getName();
								UUID uuid = p.getUniqueId();
								
								Plot plot = PlotManager.getPlotById(p, id);
								int nbrcomments = 0;
								for (String[] scom : plot.comments){
									if (scom[2].equals(uuid.toString())){
										nbrcomments = nbrcomments + 1;
									}
								}
								
								if (nbrcomments >= 2){
									p.sendMessage(Vars.Ntrl + "Oops. Only " + Vars.Neg + "2" + Vars.Ntrl + " comments can be on a single plot!");
									p.sendMessage(Vars.Ntrl + "To clear your comments: " + Vars.Neg + "/plots clearcomments");
									return true;
								}
								String text = StringUtils.join(args," ");
								text = text.substring(text.indexOf(" "));
								
								String[] comment = new String[3];
								comment[0] = playername;
								comment[1] = text;
								comment[2] = uuid.toString();
								plot.comments.add(comment);
								
								SqlManager.addPlotComment(comment, plot.comments.size(), PlotManager.getIdX(id), PlotManager.getIdZ(id), plot.world, uuid);
								
								p.sendMessage(Vars.NOTICE_PREFIX + "You have added a comment. This is what is says: ");
								p.sendMessage(Vars.NOTICE_PREFIX + text);
							}
														
						}
						else
						{
							p.sendMessage(Vars.Ntrl + "Oops. This plot does not have an owner! (ID: " + id + ")");
						}
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean comments(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.use.comments"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
				
			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. Comments aren't enabled in the Competition world!");
				return true;
			}
			else
			{
				if(args.length < 2)
				{
					String id = PlotManager.getPlotId(p.getLocation());
					
					if(id.equals(""))
					{
						p.sendMessage(C("MsgNoPlotFound"));
					}
					else
					{
						if(!PlotManager.isPlotAvailable(id, p))
						{
							Plot plot = PlotManager.getPlotById(p,id);
							
							if(plot.ownerId.equals(p.getUniqueId()) || plot.isAllowed(p.getUniqueId()) || PlotMod.hasPerms(p, "plots.admin"))
							{
								if(plot.comments.size() == 0)
								{
									p.sendMessage(Vars.NOTICE_PREFIX + "There's " + Vars.Neg + "NO" + Vars.Ntrl + " comments on this plot!");
								}
								else
								{
									
									if (plot.comments.size() <= 3){
										p.sendMessage(Vars.NOTICE_PREFIX + "There are " + Vars.Pos + plot.comments.size() + Vars.Ntrl + " comments on this plot:");
										for(String[] comment : plot.comments)
										{
											p.sendMessage(Vars.NOTICE_PREFIX + Vars.Pos + comment[0] + Vars.Ntrl + ": " + comment[1]);
										}
									} else  {
										String g = ChatColor.GOLD + "";
										String w = ChatColor.WHITE + "";
										p.sendMessage(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
										p.sendMessage(g + "There are " + w + plot.comments.size() + g + " comments on this plot:");
										for(String[] comment : plot.comments)
										{
											p.sendMessage(g + " " + comment[0] + ": " + w + comment[1]);
										}
										p.sendMessage(ChatColor.AQUA + Vars.CHAT_BAR_LINE);
										
									}
									
									
								}
							}
							else
							{
								p.sendMessage(Vars.Ntrl + "Oops. This plot is not yours- you may not see the comments! (ID: " + id + ")");
							}
						}
						else
						{
							p.sendMessage(Vars.Ntrl + "Oops. This plot does not have an owner! (ID: " + id + ")");
						}
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean biome(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.use.biome"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. Biome changing isn't allowed in the Competition world!");
				return true;
			}
			else
			{
				String id = PlotManager.getPlotId(p.getLocation());
				if(id.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					if(!PlotManager.isPlotAvailable(id, p))
					{
						World w = p.getWorld();
						
						if(args.length == 2)
						{
							Biome biome = null;
							
							for(Biome bio : Biome.values())
							{
								if(bio.name().equalsIgnoreCase(args[1]))
								{
									biome = bio;
								}
							}
							
							if(biome == null)
							{
								p.sendMessage(Vars.Ntrl + "Oops. '" + args[1] + "' is not an acceptable biome!");
								p.sendMessage(Vars.Ntrl + "For a list of biomes: " + Vars.Neg + "/plots biomelist");
							}
							else
							{
								Plot plot = PlotManager.getPlotById(p,id);
								String playername = p.getName();
								
								if(plot.owner.equalsIgnoreCase(playername) || PlotMod.hasPerms(p, "plots.admin"))
								{
									
									PlotManager.setBiome(w, id, plot, biome);
									p.sendMessage(Vars.NOTICE_PREFIX + "The biome has been set to: " + Vars.Pos + FormatBiome(biome.name(), false));
									
								
								}
								else
								{
									p.sendMessage(Vars.Ntrl + "Oops. This plot is not yours, you may not change its biome! (ID: " + id + ")");
								}
							}
						}
						else
						{
							Plot plot = PlotMod.plotmaps.get(w.getName().toLowerCase()).plots.get(id);
							
							p.sendMessage(Vars.NOTICE_PREFIX + "The biome of this plot is: " + FormatBiome(plot.biome.name(), false));
						
						}
					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. This plot does not have an owner! (ID: " + id + ")");
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean biomelist(CommandSender s, String[] args)
	{
		if (!(s instanceof Player) || PlotMod.hasPerms((Player) s, "plots.use.biome"))
		{
			
			StringBuilder line = new StringBuilder();
			List<String> biomes = new ArrayList<String>();
			
			for(Biome b : Biome.values())
			{
				biomes.add(Vars.Pos + FormatBiome(b.name(), true));
			}
			
			String comma = "";
			for (String iterating : biomes)
			{
				line.append(comma).append(iterating);
				comma = Vars.Ntrl + ", ";
			}
			
			s.sendMessage(Vars.NOTICE_PREFIX + "There are " + Vars.Pos + biomes.size() + Vars.Ntrl + " possible biomes. They are:");
			s.sendMessage(Vars.NOTICE_PREFIX + line.toString().trim());
						
		}
		else
		{
			s.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean unclaim(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.use.unclaim") || PlotMod.hasPerms(p, "plots.admin.unclaim"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
				
			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. You can't manually unclaim in the Competition world!");
				return true;
			}
			else
			{
				Plot plot = PlotManager.getPlotById(p.getLocation());
				
				if(plot == null)
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else if (PlotManager.isPlotAvailable(plot.id, p.getWorld()))
				{
					p.sendMessage(Vars.Ntrl + "Oops. This plot is currently not owned!");
				}
				else
				{
					
					if (plot.ownerId.equals(p.getUniqueId()))
					{
						if(plot.protect){
							p.sendMessage(C("MsgPlotProtectedCannotUnclaim"));
							return true;
						}

						String id = plot.id;
						World w = p.getWorld();
												
						PlotManager.setBiome(w, id, plot, Biome.PLAINS);
						PlotManager.clear(w, plot);
						
						PlotManager.getPlots(p).remove(id);
						
						PlotManager.removeOwnerSign(w, id);
						
						SqlManager.deletePlot(PlotManager.getIdX(id), PlotManager.getIdZ(id), w.getName().toLowerCase());
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You have " + Vars.Neg + "UNCLAIMED" + Vars.Ntrl + " this plot and no longer own it!");
						
					} 
					else if (PlotMod.hasPerms(p, "plots.admin.unclaim"))
					{
						if(plot.protect){
							p.sendMessage(C("MsgPlotProtectedCannotUnclaim"));
							return true;
						}
						
						String id = plot.id;
						World w = p.getWorld();
												
						PlotManager.setBiome(w, id, plot, Biome.PLAINS);
						PlotManager.clear(w, plot);
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You have " + Vars.Neg + "UNCLAIMED" + Vars.Ntrl + " " + plot.owner + "'s plot!");
						PlotManager.getPlots(p).remove(id);												
						PlotManager.removeOwnerSign(w, id);
						
						SqlManager.deletePlot(PlotManager.getIdX(id), PlotManager.getIdZ(id), w.getName().toLowerCase());

					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. This plot is not yours, you are not allowed to unclaim it. (ID: " + plot.id + ")");
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean clear(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.admin.clear") || PlotMod.hasPerms(p, "plots.use.clear"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
				
			}
			else
			{
				String id = PlotManager.getPlotId(p.getLocation());
				if(id.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					if(!PlotManager.isPlotAvailable(id, p))
					{
						Plot plot = PlotManager.getPlotById(p,id);
						
						if(plot.protect)
						{
							p.sendMessage(C("MsgPlotProtectedCannotClear"));
						}
						else
						{
							String playername = p.getName();
							
							if(plot.owner.equalsIgnoreCase(playername) || PlotMod.hasPerms(p, "plots.admin.clear"))
							{
								World w = p.getWorld();
																
								PlotManager.clear(w, plot);
								
								p.sendMessage(C("MsgPlotCleared"));
								
							}
							else
							{
								p.sendMessage(Vars.Ntrl + "Oops. This plot is not yours, you aren't allowed to clear it! (ID: " + id + ")");
							}
						}
					}
				}	
			}
		}		
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean friend(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.admin.friend") || PlotMod.hasPerms(p, "plots.use.friend"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
				
			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. You cannot have helpers in the Competition world!");
				return true;
			}
			else if (Vars.donor_plotworld != null && p.getWorld().getName().equalsIgnoreCase(Vars.donor_plotworld)){
				
				p.sendMessage(Vars.Ntrl + "Oops. You cannot have helpers in the Donator world!");
				p.sendMessage(Vars.Ntrl + "(This has been recently changed, sorry for any inconvenience!)");
				return true;
				
			}
			else
			{

				String id = PlotManager.getPlotId(p.getLocation());
				if(id.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					if(!PlotManager.isPlotAvailable(id, p))
					{
						if(args.length < 2 || args[1].equalsIgnoreCase(""))
						{
							p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " +Vars.Neg + "/plots " + C("CommandFriend") + " PLAYER");
						}
						else
						{
						
							Plot plot = PlotManager.getPlotById(p,id);
							String playername = p.getName();
							String allowed = args[1];
							
							if(plot.owner.equalsIgnoreCase(playername) || PlotMod.hasPerms(p, "plots.admin.friend"))
							{
								if(allowed.startsWith("group:") && plot.isGroupAllowed(allowed))
								{
									p.sendMessage(Vars.Ntrl + "Oops. The group '" + args[1].substring(6) + "' is already friended on this plot!");
								}
								else if(plot.isAllowedConsulting(allowed))
								{
									p.sendMessage(Vars.Ntrl + "Oops. The player '" + args[1] + "' is already allowed on this plot!");
								}
								else
								{									
									plot.addAllowed(allowed);
									plot.removeDenied(allowed);
									
									p.sendMessage(Vars.NOTICE_PREFIX + "The player " + Vars.Pos + allowed + Vars.Ntrl + " is now " + Vars.Pos + "FRIENDED" + Vars.Ntrl + "!");
									
								}
							}
							else
							{
								p.sendMessage(Vars.Ntrl + "Oops. This plot is not yours, you're not allowed to add players! (ID: " + id + ")");
							}
						}
					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. This plot is currently not owned!");
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
    private boolean deny(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.admin.deny") || PlotMod.hasPerms(p, "plots.use.deny"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							

			}
			else
			{
				String id = PlotManager.getPlotId(p.getLocation());
				if(id.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					if(!PlotManager.isPlotAvailable(id, p))
					{
						if(args.length < 2 || args[1].equalsIgnoreCase(""))
						{
							p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/plots " + C("CommandDeny") + " PLAYER");
						}
						else
						{
						
							Plot plot = PlotManager.getPlotById(p,id);
							String playername = p.getName();
							String denied = args[1];
							
							if(plot.owner.equalsIgnoreCase(playername) || PlotMod.hasPerms(p, "plots.admin.deny"))
							{
								if(plot.isDeniedConsulting(denied) || plot.isGroupDenied(denied))
								{
									if (denied.startsWith("group:")){
										p.sendMessage(Vars.Ntrl + "Oops. Group " + denied.substring(6) + " is already denied!");
									} else {
										p.sendMessage(Vars.Ntrl + "Oops. Player " + denied + " is already denied!");
									}

								}
								else
								{
									World w = p.getWorld();
																		
                                    plot.addDenied(denied);
                                    plot.removeAllowed(denied);
									
									if(denied.equals("*"))
									{
										List<Player> deniedplayers = PlotManager.getPlayersInPlot(w, id);
										
										for(Player deniedplayer : deniedplayers)
										{
											if(!plot.isAllowed(deniedplayer.getUniqueId()))
												deniedplayer.teleport(PlotManager.getPlotHome(w, plot));
										}
									}
									else
									{
                                        Player deniedplayer = null;
										Player[] onlinenow = this.plugin.getServer().getOnlinePlayers();
										for (Player iterating : onlinenow){
											if (iterating.getName().equals(denied)){
												deniedplayer = iterating;
												break;
											}
										}
										
										if(deniedplayer != null)
										{
											if(deniedplayer.getWorld().equals(w))
											{
												String deniedid = PlotManager.getPlotId(deniedplayer);
												
												if(deniedid.equalsIgnoreCase(id))
												{
													deniedplayer.teleport(PlotManager.getPlotHome(w, plot));
												}
											}
											deniedplayer.sendMessage(Vars.NOTICE_PREFIX + "You are now denied to enter one of " + p.getName() + "'s plots!");
										}
									}
									
									
									p.sendMessage(Vars.NOTICE_PREFIX + "The user(s) " + Vars.Neg + denied + Vars.Ntrl + " are now " + Vars.Neg + "DENIED" + Vars.Ntrl + " to enter this plot!");
									
								}
							}
							else
							{
								p.sendMessage("Oops. This plot is not yours, you are not allowed to deny players entry!");
							}
						}
					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. This plot does not have an owner! (ID: " + id + ")");
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
    private boolean unfriend(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.admin.unfriend") || PlotMod.hasPerms(p, "plots.use.unfriend"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
				
			}
			else if (p.getWorld().getName().equalsIgnoreCase("Competition"))
			{
				p.sendMessage(Vars.Ntrl + "Oops. Players can't build in others' plots in the first place!");
				return true;
			}
			else
			{
				String id = PlotManager.getPlotId(p.getLocation());
				if(id.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					if(!PlotManager.isPlotAvailable(id, p))
					{
						if(args.length < 2 || args[1].equalsIgnoreCase(""))
						{
							p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/plots " + C("CommandUnfriend") + " PLAYER");
						
						}
						else
						{
							Plot plot = PlotManager.getPlotById(p,id);
							UUID playeruuid = p.getUniqueId();
							String allowed = args[1];
							
							if(plot.ownerId.equals(playeruuid) || PlotMod.hasPerms(p, "plots.admin.unfriend"))
							{
								if(plot.isAllowedConsulting(allowed) || plot.isGroupAllowed(allowed))
								{
																		
                                    if (allowed.startsWith("group:")) 
                                    {
                                        plot.removeAllowedGroup(allowed);
                                        p.sendMessage(Vars.NOTICE_PREFIX + "The group " + Vars.Neg + allowed.substring(6) + Vars.Ntrl + " is now " + Vars.Neg + "UNFRIENDED" + Vars.Ntrl + "!");
                                    } 
                                    else 
                                    {
                                        plot.removeAllowed(allowed);
                                        p.sendMessage(Vars.NOTICE_PREFIX + "The player " + Vars.Neg + allowed + Vars.Ntrl + " is now " + Vars.Neg + "UNFRIENDED" + Vars.Ntrl + "!");
                                    }
																										
								}
								else
								{
									
									if (allowed.startsWith("group:")) p.sendMessage(Vars.Ntrl + "Oops. Group " + allowed.substring(6) + " already wasn't friended!");
									else p.sendMessage(Vars.Ntrl + "Oops. Player " + allowed.substring(6) + " already wasn't friended!");
								
								}
							}
							else
							{
								p.sendMessage(Vars.Ntrl + "Oops. This plot is not yours, you're not allowed to unfriend players from it! (ID: " + id + ")");
							}
						}
					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. This plot does not have an owner! (ID: " + id + ")");
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
    private boolean undeny(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.admin.undeny") || PlotMod.hasPerms(p, "plots.use.undeny"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
				
			}
			else
			{
				String id = PlotManager.getPlotId(p.getLocation());
				if(id.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					if(!PlotManager.isPlotAvailable(id, p))
					{
						if(args.length < 2 || args[1].equalsIgnoreCase(""))
						{
							p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/plots " + C("CommandDeny") + " PLAYER");
						}
						else
						{
							Plot plot = PlotManager.getPlotById(p,id);
							UUID playeruuid = p.getUniqueId();
							String denied = args[1];
							
							if(plot.ownerId.equals(playeruuid) || PlotMod.hasPerms(p, "plots.admin.undeny"))
							{
								if(plot.isDeniedConsulting(denied) || plot.isGroupDenied(denied))
								{
									
                                    if (denied.startsWith("group:")) {
                                        plot.removeDeniedGroup(denied);
										p.sendMessage(Vars.Ntrl + "Oops. Group " + denied.substring(6) + " is already denied!");
                                    } else {
                                        plot.removeDenied(denied);
										p.sendMessage(Vars.Ntrl + "Oops. Player " + denied + " is already denied!");
                                    }
																	
									
								}
								else
								{
									
                                    if (denied.startsWith("group:")) 
                                    {
                                    	p.sendMessage(Vars.Ntrl + "Oops. Group " + denied.substring(6) + " was already not denied!");
                                    }
                                    else
                                    {
                                    	p.sendMessage(Vars.Ntrl + "Oops. Player " + denied + " was already not denied!");
                                    }

								}
							}
							else
							{
								p.sendMessage(Vars.Ntrl + "Oops. This plot is not yours, you aren't allowed to undeny players! (ID: " + id + ")");
							}
						}
					}
					else
					{
						p.sendMessage(Vars.Ntrl + "Oops. This plot does not have an owner! (ID: " + id + ")");
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean setowner(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.admin.setowner"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
				
			}
			else
			{
				String id = PlotManager.getPlotId(p.getLocation());
				if(id.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					if(args.length < 2 || args[1].equals(""))
					{
						p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/plots " + C("CommandSetowner") + " PLAYER");
					}
					else
					{
						String newowner = args[1];
						
						if(!PlotManager.isPlotAvailable(id, p))
						{								
							Plot plot = PlotManager.getPlotById(p,id);
												
							plot.owner = newowner;
							
							PlotManager.setOwnerSign(p.getWorld(), plot);
							
							plot.updateField("owner", newowner);
						}
						else
						{
							PlotManager.createPlot(p.getWorld(), id, newowner, null);
						}
						
						p.sendMessage(Vars.NOTICE_PREFIX + "The owner of this plot is now " + Vars.Pos + newowner + Vars.Ntrl + ".");
												
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean id(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.admin.id"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
				
			}
			else
			{
				String plotid = PlotManager.getPlotId(p.getLocation());
				
				if(plotid.equals(""))
				{
					p.sendMessage(C("MsgNoPlotFound"));
				}
				else
				{
					p.sendMessage(Vars.NOTICE_PREFIX + "The ID of this plot is: " + Vars.Pos + plotid);
					
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean move(Player p, String[] args)
	{
		if (PlotMod.hasPerms(p, "plots.admin.move"))
		{
			if(!PlotManager.isPlotWorld(p))
			{
				p.sendMessage(Vars.Ntrl + "Oops. The world you're in- '" + p.getWorld().getName() + "' -is not a plot world!");
				p.sendMessage(Vars.Ntrl + "To go to a plot world use either: " + Vars.Neg + "/donator" + Vars.Ntrl + " or " + Vars.Neg + "/regular");							
				
			}
			else
			{
				if(args.length < 3 || args[1].equalsIgnoreCase("") || args[2].equalsIgnoreCase(""))
				{
					p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/plots " + C("CommandMove") + " FROM-ID TO-ID");
				}
				else
				{
					String plot1 = args[1];
					String plot2 = args[2];
					
					if(!PlotManager.isValidId(plot1) || !PlotManager.isValidId(plot2))
					{
						
						p.sendMessage(Vars.Ntrl + "Oops. The ID format wasn't correct. Should be #,# EXAMPLE: 1,1");
						return true;
					}
					else
					{
						if(PlotManager.movePlot(p.getWorld(), plot1, plot2))
						{
							p.sendMessage(Vars.NOTICE_PREFIX + "The two plots have been moved " + Vars.Pos + "SUCCESSFULLY" + Vars.Ntrl + "!");							
						}
						else
						{
							p.sendMessage(Vars.NOTICE_PREFIX + "Plots " + Vars.Neg + "FAILED" + Vars.Ntrl + " to cooperate and could not be moved!");
						}
							
					}
				}
			}
		}
		else
		{
			p.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private boolean reload(CommandSender s, String[] args)
	{
		if (!(s instanceof Player) || PlotMod.hasPerms((Player) s, "plots.admin.reload"))
		{
			plugin.initialize();
			s.sendMessage(C("MsgReloadedSuccess"));
			
		}
		else
		{
			s.sendMessage(C("MsgPermissionDenied"));
		}
		return true;
	}
	
	private String FormatBiome(String biome, boolean includeunderscores)
	{
		biome = biome.toLowerCase();
		
		String[] tokens = biome.split("_");
		
		biome = "";
		
		for(String token : tokens)
		{
			
			token = token.substring(0, 1).toUpperCase() + token.substring(1);
			
			if(biome.equalsIgnoreCase(""))
			{
				biome = token;
			}
			else
			{
				if (includeunderscores){
					biome = biome + "_" + token;
				} else {
					biome = biome + " " + token;
				}
			}
		}

		return biome;
	}
	
	public boolean sendToRegularWorld(Player p, boolean tellarrival){
		
		MultiverseCore mvcore = (MultiverseCore) this.plugin.getServer().getPluginManager().getPlugin("Multiverse-Core");
		if (mvcore != null){
			MultiverseWorld mvw = mvcore.getMVWorldManager().getMVWorld(Vars.regular_plotworld);
			if (mvw == null){
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "ERROR. REGULAR PLOTS SPAWN COULDN'T BE FOUND.");
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "TELL AN ADMIN IMMEDIATELY!");
				return false;
			} else {
				mvcore.getSafeTTeleporter().safelyTeleport(p, p, mvw.getSpawnLocation(), true);
				if (tellarrival) p.sendMessage(Vars.NOTICE_PREFIX + "You are now at the " + Vars.Pos + "REGULAR " + Vars.Ntrl + " plots spawn!");
			}

		} else {
			
			World w = Bukkit.getServer().getWorld(Vars.regular_plotworld);
			if (w == null){
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "ERROR. REGULAR PLOTS SPAWN COULDN'T BE FOUND.");
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + "TELL AN ADMIN IMMEDIATELY!");
				return false;
			} else {
				
				p.teleport(w.getSpawnLocation());
				if (tellarrival) p.sendMessage(Vars.NOTICE_PREFIX + "You are now at the " + Vars.Pos + "REGULAR " + Vars.Ntrl + "plots spawn!");
				
			}
		}
		return true;
		
	}
	
}
